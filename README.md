### Due: 20.09.19 (23:59 CET)

# AutoML_SS19_project

Repository with the baseline code for the AutoML SS19 final project (https://ilias.uni-freiburg.de/ilias.php?ref_id=1264546&cmdClass=ilrepositorygui&cmdNode=vs&baseClass=ilrepositorygui)


# How to run!?
The main code is in the file called LevelBasedAutoML.py

LevelBasedAutoML.py is expected to be ran in the src directory and to create:
    A memory db file with a registry of all the runs
    log files under the name of every component (A new directory would be created for each component)
    Overall logging is printed in stdout also

Expected Runtime depends on the configuration of the Level Based AutoML file, particularly:
     steps = [
       {'type':'NAS', 'dataset':'K49', 'dataset_reduction':0.7, 'max_budget':5, 'iterations':10},
       {'type':'HPO', 'dataset':'K49', 'dataset_reduction':0.5, 'max_budget':5, 'iterations':12},
       {'type':'NAS', 'dataset':'K49', 'dataset_reduction':0.4, 'max_budget':10, 'iterations':3},
       {'type':'HPO', 'dataset':'K49', 'dataset_reduction':0,   'max_budget':20, 'iterations':8},
     ]

     This means that 4 levels are going to be launched with different dataset reduction and budgets.
     More/less can be added to this variable. The current setting for instance takes 20 hours of GPU time on average,
     but even if for some reason it takes more than 24 hours the anytime performance can actively be checked in the memory.db with sqlite3
