import cv2
import numpy as np
import calendar
import time
import os
import json
import utils
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader, Dataset
import torchvision
import torchvision.transforms as transforms
from torchsummary import summary
from tqdm import tqdm
from CollectiveMemory import CollectiveMemory
from datasets import K49, KMNIST
from BaseTorchCNNClass import build_fn, train_fn, eval_fn
from utils import make_reproducible_run
from transformations import CV2threshold, medianBlur, GaussianBlur

import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import hpbandster.core.result as hpres
import hpbandster.visualization as hpvis
import hpbandster.core.nameserver as hpns
from hpbandster.optimizers import BOHB
from multiprocessing import Process, Queue


#--- Create a custom logger
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter('[%(name)s,%(levelname)s]: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class BOHBWorker(Worker):
    def __init__(
        self,
        train_dataset : Dataset,
        valid_dataset : Dataset,
        seed: int,
        max_budget: int,
        config_type: str,
        **kwargs,
    ):
        """
        This class trains a neural network based on a collective memory of processes.
        Particularly, the method is the worker that builds and run the neural network
        """
        super().__init__(**kwargs)
        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset
        self.seed = seed
        self.max_budget = max_budget
        self.config_type = config_type

        # Device configuration
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        #--- Make run reproducible
        make_reproducible_run(self.seed)


    def compute(
        self,
        config : dict,
        budget: int,
        working_directory: str,
        force_save : bool = False,
        *args,
        **kwargs,
    ):
        """
        This function computes a CNN to optimize based on a base clase CNN
        The input parameter "config" (dictionary) contains the sampled configurations passed by the bohb optimizer
        """

        #--- if this is a NAS BOHB then store this info
        if self.config_type != 'HPO' and self.config_type != 'PREPROCESSING':
            config["NAS"] = "BOHB"

        #--- Speak the same language across components
        memory = CollectiveMemory()

        #--- Because this BOHB can be used for both HPO and NAS
        #--- We expect that some configs would be missings
        #--- Proactively take them from the best run
        config = memory.complete_config(config)
        logger.info(f"Running job with config {config}")

        #--- Handle data transformations if there are some in the config
        transformations = [transforms.ToTensor()]
        for transform in memory.preprocessing:
            if transform in config and config[transform]:
                if 'GaussianBlur' in transform:
                    transformations.insert(0,GaussianBlur())
                if 'CV2threshold' in transform:
                    transformations.insert(0,CV2threshold())
                if 'medianBlur' in transform:
                    transformations.insert(0,medianBlur())
        if len(transformations) > 2:
            self.train_dataset.transform = torchvision.transforms.Compose(transformations)
            self.valid_dataset.transform = torchvision.transforms.Compose(transformations)

        #--- handle data loading given the dataset
        train_loader = DataLoader(dataset=self.train_dataset,
                          batch_size=int(config['batch_size']),
                          pin_memory=True,
                          shuffle=True)
        valid_loader = DataLoader(dataset=self.valid_dataset,
                         batch_size=int(config['batch_size']),
                         pin_memory=True,
                         shuffle=False)

        #--- Build the model
        try:
            model = build_fn(config, device=self.device, dataset = self.train_dataset)

        except RuntimeError as e:
            #--- Discourage run as it breaks the code
            logger.error(f"Run for config={config} broke the CNN for {e}")
            return ({
                    'loss': 1, # remember: HpBandSter always minimizes!
                    'info': {       'score': 0,
                                    'save_path' : None,
                                    'accuracy_history' : '',
                                    'config' : config
                                    }
            })

        #--- Check if there is a similar run to warm start weights
        #--- create an search_config to search for similar runs with same
        #--- architectural config to proactively use
        search_config = {}
        for arch_item in list(set(memory.arch_config) & set(config.keys())):
            search_config[arch_item] = config[arch_item]
        similar_runs = memory.get_runs_by_config(search_config, filters=['save_path'])
        if similar_runs.size:
            logger.debug(f"Found a similar run to {config} being {similar_runs}")
            model.warmstart_weights(similar_runs)

        #--- Train the model
        accuracy_history = train_fn(model, config, train_loader, self.device, budget, verbose=False)

        #--- Get how good was this model
        valid_score = eval_fn(model,loader=valid_loader,device=self.device)

        #--- If it is a full budget run, save it
        save_path = None
        if budget == self.max_budget or force_save or valid_score > memory.highest_score*0.9:
            os.makedirs('models', exist_ok=True)
            save_path = 'bohb_%.3f_'%(valid_score) + str(os.getpid()) + "_" + str(calendar.timegm(time.gmtime())) + '.pt'
            save_path  = os.path.join('models', save_path)
            torch.save(model, save_path)

        return ({
                'loss': 100-valid_score, # remember: HpBandSter always minimizes!
                'info': {       'score': valid_score,
                                'save_path' : save_path,
                                'accuracy_history' : ''.join('%.3f,'%(e) for e in accuracy_history),
                                'config' : config
                                }
        })


    @staticmethod
    def get_configspace(config_type):
            """
            It builds the configuration space with the needed hyperparameters.
            The class CollectiveMemory can be used as a base class to get the config space
            :return: ConfigurationsSpace-Object
            """
            return CollectiveMemory().getConfigSpace(config_type)


class BOHBComponent(Process):

    def __init__(
        self,
        run_id : str,
        iterations : int,
        dataset : str,
        seed: int,
        max_budget : int,
        queue: Queue,
        dataset_reduction : float = None,
        data_dir : str = '../data',
        overwrite : bool = False,
        previous_run : str = None,
        config_type: str = 'ALL',
        **kwargs,
    ):
        """
        BOHB object that is supposed to run in a separated CPU with a run method
        """
        super(BOHBComponent, self).__init__()

        #--- Handle the dataset to be created
        self.dataset = dataset
        self.data_dir = data_dir
        self.dataset_reduction = dataset_reduction
        self.load_data()

        #--- get all runs that have been launched as a pandas frame
        self.memory = CollectiveMemory()
        self.run_id = run_id
        self.run_dir = f"{run_id}/"
        self.max_budget = max_budget
        self.seed = seed
        self.iterations = iterations
        self.overwrite = overwrite
        self.previous_run = previous_run
        self.queue = queue
        self.config_type = config_type
        self.w = None

    def load_data(self):
        """Loads the data of the model"""
        data_augmentations = transforms.ToTensor()
        if self.dataset == 'K49':
            self.train_dataset = K49(self.data_dir, data_augmentations, self.dataset_reduction, 'train')
            self.valid_dataset = K49(self.data_dir, data_augmentations, self.dataset_reduction, 'valid')
        elif self.dataset == 'KMNIST':
            self.train_dataset = KMNIST(self.data_dir, data_augmentations, self.dataset_reduction, 'train')
            self.valid_dataset = KMNIST(self.data_dir, data_augmentations, self.dataset_reduction, 'valid')
        else:
            raise Exception("Unsuported dataset provided not in {k49, KMNIST}")

    def get_iteration(self):
        """Get the current iteration for the progress bar tracking"""
        filename = self.run_dir + '/results.json'
        if not os.path.isfile(filename):
            return 0

        with open(filename, 'r') as f:
            lines = f.read().splitlines()
            #--- File is not yet populated!
            if len(lines) <= 0:
                return 0
            last_line_id = lines[-1]
            #--- Only empty lines
            if last_line_id.isspace():
                return 0
            return int(last_line_id.split(',')[0].replace('[',''))


    def run(self):
        """Overwrite Method to allow the BOHB object to run"""

        logger.info(f"Started a BOHB process called {self.run_id} with number {self.pid} and max_budget={self.max_budget}")

        #--- Looking for reproducibility
        make_reproducible_run(self.seed)

        #--- store the results, key to restart!
        result_logger = hpres.json_result_logger(directory=self.run_dir, overwrite=self.overwrite)

        # Start a nameserver:
        NS = hpns.NameServer(run_id=self.run_id, host='127.0.0.1', port=0, working_directory=self.run_dir)
        ns_host, ns_port = NS.start()

        # Start local worker
        self.w = BOHBWorker(
            run_id=self.run_id,
            host='127.0.0.1',
            nameserver=ns_host,
            nameserver_port=ns_port,
            timeout=120,
            train_dataset = self.train_dataset,
            valid_dataset = self.valid_dataset,
            seed = self.seed,
            max_budget=self.max_budget,
            config_type=self.config_type,
        )
        self.w.run(background=True)

        if self.previous_run:
            self.previous_run = hpres.logged_results_to_HBS_result(self.previous_run)

        bohb = BOHB(  configspace = self.w.get_configspace(self.config_type),
            run_id = self.run_id,
            host = '127.0.0.1',
            nameserver = ns_host,
            nameserver_port = ns_port,
            result_logger = result_logger,
            min_budget = 1,
            max_budget = self.max_budget,
            previous_result = self.previous_run,
        )

        res = bohb.run(n_iterations=self.iterations)

        self.memorize_bohb_results()

        # let's grab the run on the highest budget
        id2conf = res.get_id2config_mapping()
        inc_id = res.get_incumbent_id()
        inc_runs = res.get_runs_by_id(inc_id)
        inc_run = inc_runs[-1]
        inc_config = id2conf[inc_id]['config']
        score = inc_run.info['score']

        # shutdown
        bohb.shutdown(shutdown_workers=True)
        NS.shutdown()

        self.queue.put([score,inc_config])

        return

    def warmstap_with_memory(self):
        """
        Capability to warmstart BOHB with the collective memory
        """

        #--- Update the memory just in case something new came up
        self.memory.update()

        #--- generate the area to trick BOHB
        os.makedirs(self.run_dir, exist_ok=True)
        config_fn  = os.path.join(run_dir, 'configs.json')
        results_fn = os.path.join(run_dir, 'results.json')

        for row in range(self.memory.memory.shape[0]):
            dict_row = self.memory.memory.iloc[row].fillna(0).to_dict()
            budget = int(dict_row['budget'])
            timestamps = {"submitted": dict_row['start'], "started": dict_row['start'], "finished":dict_row['end']}
            score = dict_row['accuracy']
            save_path = dict_row['save_path']
            result = {"loss": 1-score, "info": {"score": score, "save_path":save_path}}
            config_id = [int(dict_row['index'])]*3
            config_info = {"model_based_pick": 'false'}
            config = self.memory.prune_config(dict_row)

            with open(config_fn, 'a') as fh:
                fh.write(json.dumps([config_id, config, config_info]))
                fh.write('\n')

            with open(results_fn, 'a') as fh:
                fh.write(json.dumps([config_id, budget, timestamps, result, 'null']))
                fh.write("\n")

    def memorize_bohb_results(self):
        """
        Store the finished Bohb Run in the collective memory
        """
        results = hpres.logged_results_to_HBS_result(self.run_dir)
        time_ref = results.HB_config['time_ref']
        all_runs = results.get_all_runs()
        if not isinstance(all_runs, list): all_runs = [all_runs]
        id2conf = results.get_id2config_mapping()

        #--- append into the collective memory for the next run
        for datum in all_runs:
            #--- Ignore failed runs
            if datum.info is None:
                continue
            config = datum.info['config']
            config['budget'] = datum.budget
            config['accuracy'] = datum.info['score']
            config['save_path'] = datum.info['save_path']
            config['accuracy_history'] = datum.info['accuracy_history']
            config['start'] = time_ref + datum.time_stamps['started']
            config['end'] = time_ref + datum.time_stamps['finished']
            config['seed'] = self.seed
            config['method'] = self.run_id

            #--- It could be that this method is HPO search
            for element in self.memory.arch_config:
                if element not in config:
                    config[element] = None
            self.memory.addConfigMemory(config)

        #--- Save in the database
        self.memory.save()

if __name__ == "__main__":

    """Check the functionality of all the options here"""
    q = Queue()
    for config_type in ['HPO', 'NAS', 'ALL']:
        bohb_test = BOHBComponent(
            run_id = "test" + config_type,
            config_type = config_type,
            iterations = 1,
            dataset = 'KMNIST',
            dataset_reduction = 0.9,
            seed = 56,
            max_budget = 1,
            queue = q,
            overwrite = True
        )
        bohb_test.run()
        print("\n\n\n")
        print('#'*40)
        print(f"BOHB {config_type} Result:")
        print('#'*40)
        print(q.get())
        print('#'*40, "\n\n\n")
