import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.autograd import Variable
from tqdm import tqdm
from utils import AvgrageMeter, accuracy
from cnn import torchModel
import os
from torchsummary import summary
#from macro_decoder import ResidualGenomeDecoder
from abc import ABC, abstractmethod
from collections import OrderedDict, namedtuple
from CollectiveMemory import CollectiveMemory
from torch.utils.data import DataLoader, Dataset
from datasets import K49, KMNIST
import pickle
import utils
import torchvision.transforms as transforms
import re, ast

#--- Create a custom logger
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter('[%(name)s,%(levelname)s]: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


#############################################################################################
#                                SECTION 1: General functions
#############################################################################################
def build_fn(config, device=None, dataset = None):
    """
    This functions build a model based on the contents of the config
    """

    if device is None:
        device = 'cuda' if torch.cuda.is_available() else 'cpu'

    #--- Assume that if the loader is non, we want to see final performance
    if dataset is None:
        data_augmentations = transforms.ToTensor()
        dataset = K49("../data", data_augmentations, None, 'test')

    #--- Build the model base on config contents
    if 'BOHB' in config["NAS"]:
        model = BaseTorchCNNClass(
            config,
            input_shape=(
                dataset.channels,
                dataset.img_rows,
                dataset.img_cols
            ),
            num_classes=dataset.n_classes
        ).to(device)
    elif 'NSGA_macro' in config["NAS"]:
        #genome = pickle.loads(config['genome'])
        genome = np.array(ast.literal_eval(config['genome']))
        genotype = utils.decode_macro(genome)
        init_channels = int(config['init_channels'])
        channels = [(1, init_channels),
                 (init_channels, 2*init_channels),
                 (2*init_channels, 4*init_channels)]
        model = MacroNetwork(
                genotype,
                channels,
                dataset.n_classes,
                (dataset.img_rows, dataset.img_cols),
                decoder='residual'
        ).to(device)
    elif 'NSGA_micro' in config["NAS"]:
        #genome = pickle.loads(config['genome'])
        genome = np.array(ast.literal_eval(config['genome']))
        genotype = utils.decode_micro(genome)
        init_channels = int(config['init_channels'])
        auxiliary = False
        channels = [(1, init_channels),
                 (init_channels, 2*init_channels),
                 (2*init_channels, 4*init_channels)]
        model = MicroNetwork(
                int(config['init_channels']),
                dataset.n_classes,
                int(config['layers']),
                auxiliary,
                genotype,
        ).to(device)
    else:
        raise Exception(f"Model architecture cannot be determined from config={config}")

    return model

def train_fn(model, config, loader, device, epochs, droprate = None, verbose=False):
    """
    Training method
    :param config: To build different capabilities on the fly, like optim
    :criterion: loss function
    :param loader: data loader for either training or testing set
    :param device: torch device
    :param train: boolean to indicate if training or test set is used
    :return: (accuracy, loss) on the data
    """
    score = AvgrageMeter()
    objs = AvgrageMeter()

    #--- Enable learning curve prediction to save tpt
    memory = CollectiveMemory()

    #--- Set the model in training mode
    model.train()

    #--- Show the model if verbose
    if verbose:
        summary(model, (1,28,28),
             device='cuda' if torch.cuda.is_available() else 'cpu')

    #--- Add early stopping support
    es = EarlyStopping(patience=5, mode="max")

    #--- Build train dependencies
    criterion = torch.nn.CrossEntropyLoss().to(device)

    #--- Adapt to the optimizer
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    if config['optimizer'] == 'Adam':
        optimizer = torch.optim.Adam(parameters, lr=config['lr'], betas=(config['beta0'],    config['beta1']), weight_decay=float(config['weight_decay']))
    elif config['optimizer'] == 'Adadelta':
        optimizer = torch.optim.Adadelta(parameters, lr=config['lr'], rho=config['rho'],     weight_decay=float(config['weight_decay'] ))
    elif config['optimizer'] == 'SGD':
        optimizer = torch.optim.SGD(parameters, lr=config['lr'],                             momentum=config['sgd_momentum'], weight_decay=float(config['weight_decay']))
    elif config['optimizer'] == 'RMSprop':
        optimizer = torch.optim.RMSprop(parameters, lr=config['lr'], alpha=config['alpha'],  momentum=config['rms_momentum'], weight_decay=float(config['weight_decay'] ))
    else:
        raise Exception(f"Provided optimizer {config['optimizer']} is not supported")

    #--- Configure the Scheduler
    if config['scheduler'] == 'StepLR':
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.1)
    elif config['scheduler'] == 'ExponentialLR':
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.1)
    elif config['scheduler'] == 'CosineAnnealingLR':
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=int(epochs))

    #--- Train the model
    loss_history = []
    accuracy_history = []
    for epoch in range(1,int(epochs)+1):

        if droprate:
            model.droprate = droprate * epoch / epochs

        t = loader
        if verbose: t = tqdm(loader)
        for images, labels in t:
            images = images.to(device)
            labels = labels.to(device)

            optimizer.zero_grad()
            logits, outputs_aux = model(images)
            loss = criterion(logits, labels)

            #--- Auxiliary tower support
            if outputs_aux:
                loss_aux = criterion(outputs_aux, targets)
                loss += 0.4 * loss_aux

            loss.backward()
            #nn.utils.clip_grad_norm_(model.parameters(), 5)
            optimizer.step()

            acc, _ = accuracy(logits, labels, topk=(1, 5))
            n = images.size(0)
            objs.update(loss.item(), n)
            score.update(acc.item(), n)

            if verbose: t.set_description('(=> Training) Loss: {:.4f}'.format(objs.avg))

        #--- for tracking
        train_score, train_loss = score.avg, objs.avg
        accuracy_history.append(train_score)
        loss_history.append(train_loss)

        if config['scheduler']:
            #--- update the scheduler
            scheduler.step()

        #--- Proactively terminate run early if it a bad run
        if epoch in list(memory.lc_pred_model.keys()):
            prediction = memory.lc_pred_model[epoch].predict(np.array(accuracy_history).reshape(1,epoch))
            if prediction < 0.4:
                logging.debug(f"Early termination at {epoch} epochs with predict {prediction} and accuracy history of {accuracy_history}")
                break
    return accuracy_history

def eval_fn(model, loader=None, device=None, verbose=False):
    """
    Evaluation method
    :param loader: data loader for either training or testing set
    :param device: torch device
    :param train: boolean to indicate if training or test set is used
    :return: accuracy on the data
    """

    if device is None:
        device = 'cuda' if torch.cuda.is_available() else 'cpu'

    #--- Show the model if verbose
    if verbose:
        summary(model, (1,28,28), device=device)

    #--- Assume that if the loader is non, we want to see final performance
    if loader is None:
        data_augmentations = transforms.ToTensor()
        test_dataset = K49("../data", data_augmentations, None, 'test')
        loader = DataLoader(dataset=test_dataset,
                          batch_size=96,
                          pin_memory=True,
                          shuffle=False)

    score = AvgrageMeter()
    model.eval()

    t = loader
    if verbose: t = tqdm(loader)
    with torch.no_grad():  # no gradient needed
        for images, labels in t:
            images = images.to(device)
            labels = labels.to(device)

            outputs, outputs_aux = model(images)
            acc, _ = accuracy(outputs, labels, topk=(1, 5))
            score.update(acc.item(), images.size(0))

            if verbose: t.set_description('(=> Test) Score: {:.4f}'.format(score.avg))

    return score.avg

def print_fn():
    """Prints the best run in memory in a nice format"""

    #--- Enable learning curve prediction to save tpt
    memory = CollectiveMemory()

    print('#'*60)
    print(f"#\tInformation about the best found Run")
    print('#'*60)
    print(memory.memory.iloc[memory.memory['accuracy'].idxmax()].transpose())
    print('#'*60)

    save_path = memory.memory.iloc[memory.memory['accuracy'].idxmax()]['save_path']
    if utils.is_valid(save_path) and os.path.isfile(save_path):
        model = torch.load(save_path)
    else:
        model = build_fn(memory.get_best_run_dict())
    accuracy = eval_fn(model, loader=None, device=None, verbose=True)

    print(f"\nThe best run accuracy is = {accuracy}")

class EarlyStopping(object):
    """Early stopping from Pytorch"""
    """Taken from https://gist.github.com/stefanonardo/693d96ceb2f531fa05db530f3e21517d"""
    def __init__(self, mode='min', min_delta=0, patience=10, percentage=False):
        self.mode = mode
        self.min_delta = min_delta
        self.patience = patience
        self.best = None
        self.num_bad_epochs = 0
        self.is_better = None
        self._init_is_better(mode, min_delta, percentage)

        if patience == 0:
            self.is_better = lambda a, b: True
            self.step = lambda a: False

    def step(self, metrics):
        if self.best is None:
            self.best = metrics
            return False

        if np.isnan(metrics):
            return True

        if self.is_better(metrics, self.best):
            self.num_bad_epochs = 0
            self.best = metrics
        else:
            self.num_bad_epochs += 1

        if self.num_bad_epochs >= self.patience:
            return True

        return False

    def _init_is_better(self, mode, min_delta, percentage):
        if mode not in {'min', 'max'}:
            raise ValueError('mode ' + mode + ' is unknown!')
        if not percentage:
            if mode == 'min':
                self.is_better = lambda a, best: a < best - min_delta
            if mode == 'max':
                self.is_better = lambda a, best: a > best + min_delta
        else:
            if mode == 'min':
                self.is_better = lambda a, best: a < best - (
                            best * min_delta / 100)
            if mode == 'max':
                self.is_better = lambda a, best: a > best + (
                            best * min_delta / 100)

#############################################################################################
#                                SECTION 2: Base CNN
#############################################################################################
class BaseTorchCNNClass(torchModel):
    def __init__(self, config, input_shape=(1, 28, 28), num_classes=10):
        self.input_shape = input_shape
        super(torchModel, self).__init__()
        kernel_size = int(config['kernel_size'])

        ########################################
        # CONVOLUTIONS
        ########################################
        in_channels = input_shape[0]
        layers = []
        #--- For every conv layer determined by num_conv_layers, query the desired num of filters
        for out_dim in [int(config["num_filters_" + str(i)]) for i in range(int(config['num_conv_layers']))]:

            #--- We would a conv2d
            layers.extend([nn.Conv2d(in_channels, out_dim, kernel_size=kernel_size, stride=2, padding=1)])

            #--- Add a batch norm 2d if config request so
            if config['BatchNorm2d']:
                layers.extend([nn.BatchNorm2d(out_dim)])

            #--- The activation depends on the config, so handle valid cases
            if config['activation'] == 'ReLU':
                layers.extend([nn.ReLU(inplace=False)])
            elif config['activation'] == 'LeakyReLU':
                layers.extend([nn.LeakyReLU(inplace=False)])
            elif config['activation'] == 'Tanh':
                layers.extend([nn.Tanh()])
            else:
                raise Exception(f"Provided activation {config['activation']} is not supported")

            #--- Add Max Pooling
            layers.extend([nn.MaxPool2d(kernel_size=kernel_size, stride=1)])

            #--- Add dropout if requested
            if config['dropout']:
                layers.extend([nn.Dropout(p=config['dropout'])])

            #--- Update channels for the enxt iteration
            in_channels = out_dim

        self.conv_layers = nn.Sequential(*layers)

        ########################################
        # FULLY CONNECTED
        ########################################
        input_dim = self._get_conv_output(input_shape)
        layers = []
        for out_dim in [int(config["num_fc_outputs_" + str(i)]) for i in range(int(config['num_fc_layers']))]:

            #--- We would a fully connected layer
            layers.extend([nn.Linear(input_dim, out_dim)])

            #--- Add a batch norm 2d if config request so
            if config['BatchNorm1d']:
                layers.extend([nn.BatchNorm1d(out_dim)])

            #--- The activation depends on the config, so handle valid cases
            if config['activation'] == 'ReLU':
                layers.extend([nn.ReLU(inplace=False)])
            elif config['activation'] == 'LeakyReLU':
                layers.extend([nn.LeakyReLU(inplace=False)])
            elif config['activation'] == 'Tanh':
                layers.extend([nn.Tanh()])
            else:
                raise Exception(f"Provided activation {config['activation']} is not supported")

            #--- Update channels for the enxt iteration
            input_dim = out_dim

        self.fc_layers = nn.Sequential(*layers)

        self.last_fc = nn.Linear(input_dim, num_classes)


    def _get_conv_output(self, shape):
        # generate input sample and forward to get shape
        bs = 1
        input = Variable(torch.rand(bs, *shape))
        output_feat = self.conv_layers(input)
        n_size = output_feat.data.view(bs, -1).size(1)
        return int(n_size)

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        x = self.fc_layers(x)
        x = self.last_fc(x)
        return x, None

    def warmstart_weights(self,table_of_runs):
        """
        As of now support only reading params exactly from other run
        """

        #--- Just take the first row regardless which is the best accuracy model so far
        save_path = table_of_runs.iloc[[0]]['save_path'].item()
        if not os.path.isfile(save_path):
            logger.error(f"ERROR cannot open file {save_path} for weight warm start")
            return

        pretrained_model = torch.load(save_path)
        pretrained_dict = pretrained_model.state_dict()

        # 1. filter out unnecessary keys
        #pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in self.state_dict()}
        #--- Looks like batch norm add an extra layer, so we can not just replace
        for name, param in pretrained_dict.items():
            if name not in self.state_dict():
                continue
            if isinstance(param, nn.Parameter):
                # backwards compatibility for serialized parameters
                param = param.data
            self.state_dict()[name].copy_(param)

        # 2. overwrite entries in the existing state dict
        self.state_dict().update(self.state_dict())
        # 3. load the new state dict
        self.load_state_dict(self.state_dict())

        return

#############################################################################################
#                            SECTION 3: Macro Genetic Network
#############################################################################################
def phase_active(gene):
    """
    Determine if a phase is active.
    :param gene: list, gene describing a phase.
    :return: bool, true if active.
    """
    #--- The residual bit is not relevant in if a phase is active,
    #--- so we ignore it, i.e. gene[:-1].
    return sum([sum(t) for t in gene[:-1]]) != 0

class Decoder(ABC):
    """
    Abstract genome decoder class.
    """

    @abstractmethod
    def __init__(self, list_genome):
        """
        :param list_genome: genome represented as a list.
        """
        self._genome = list_genome

    @abstractmethod
    def get_model(self):
        raise NotImplementedError()


class ChannelBasedDecoder(Decoder):
    """
    Channel based decoder that deals with encapsulating constructor logic.
    """

    def __init__(self, list_genome, channels, repeats=None):
        """
        Constructor.
        :param list_genome: list, genome describing the connections in a network.
        :param channels: list, list of tuples describing the channel size changes.
        :param repeats: None | list, list of integers describing how many times to repeat each phase.
        """
        super().__init__(list_genome)

        self._model = None

        #--- First, we remove all inactive phases.
        self._genome = self.get_effective_genome(list_genome)
        self._channels = channels[:len(self._genome)]


        #--- Use the provided repeats list,
        #--- or a list of all ones (only repeat each phase once).
        if repeats is not None:
            #--- First select only the repeats that are active in the list_genome.
            active_repeats = []
            for idx, gene in enumerate(list_genome):
                if phase_active(gene):
                    active_repeats.append(repeats[idx])

            self.adjust_for_repeats(active_repeats)
        else:
            # Each phase only repeated once.
            self._repeats = [1 for _ in self._genome]

        # If we had no active nodes, our model is just the identity, and we stop constructing.
        if not self._genome:
            self._model = Identity()


    def adjust_for_repeats(self, repeats):
        """
        Adjust for repetition of phases.
        :param repeats:
        """
        self._repeats = repeats

        #--- Adjust channels and genome to agree with repeats.
        repeated_genome = []
        repeated_channels = []
        for i, repeat in enumerate(self._repeats):
            for j in range(repeat):
                if j == 0:
                    #--- This is the first instance of this repeat,
                    #--- we need to use the (in, out) channel   convention.
                    repeated_channels.append((self._channels[i][0], self._channels[i][1]))
                else:
                    #--- This is not the first instance, use the (out, out) convention.
                    repeated_channels.append((self._channels[i][1], self._channels[i][1]))

                repeated_genome.append(self._genome[i])

        self._genome = repeated_genome
        self._channels = repeated_channels

    def build_layers(self, phases):
        """
        Build up the layers with transitions.
        :param phases: list of phases
        :return: list of layers (the model).
        """
        layers = []
        last_phase = phases.pop()
        for phase, repeat in zip(phases, self._repeats):
            for _ in range(repeat):
                layers.append(phase)
            # TODO: Generalize this, or consider  a new genome.
            layers.append(nn.MaxPool2d(kernel_size=2, stride=2))

        layers.append(last_phase)
        return layers

    @staticmethod
    def get_effective_genome(genome):
        """
        Get only the parts of the genome that are active.
        :param genome: list, represents the genome
        :return: list
        """
        return [gene for gene in genome if phase_active(gene)]

    @abstractmethod
    def get_model(self):
        raise NotImplementedError()



class ResidualNode(nn.Module):
    """
    Basic computation unit.
    Does convolution, batchnorm, and relu (in this order).
    """

    def __init__(self, in_channels, out_channels, stride=1,
                 kernel_size=3, padding=1, bias=False):
        """
        Constructor.
        Default arguments preserve dimensionality of input.

        :param in_channels: input to the node.
        :param out_channels: output channels from the node.
        :param stride: stride of convolution, default 1.
        :param kernel_size: size of convolution kernel, default 3.
        :param padding: amount of zero padding, default 1.
        :param bias: true to use bias, false to not.
        """
        super(ResidualNode, self).__init__()

        self.model = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=stride,                padding=padding, bias=bias),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        """
        Apply forward propagation operation.
        :param x: Variable, input.
        :return: Variable.
        """
        return self.model(x)

class PreactResidualNode(nn.Module):
    """
    Basic computation unit.
    Does batchnorm, relu, and convolution (in this order).
    """

    def __init__(self, in_channels, out_channels, stride=1,
                 kernel_size=3, padding=1, bias=False):
        """
        Constructor.
        Default arguments preserve dimensionality of input.

        :param in_channels: input to the node.
        :param out_channels: output channels from the node.
        :param stride: stride of convolution, default 1.
        :param kernel_size: size of convolution kernel, default 3.
        :param padding: amount of zero padding, default 1.
        :param bias: true to use bias, false to not.
        """
        super(PreactResidualNode, self).__init__()

        self.model = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, stride=stride,                padding=padding, bias=bias)
        )

    def forward(self, x):
        """
        Apply forward propagation operation.
        :param x: Variable, input.
        :return: Variable.
        """
        return self.model(x)

class Identity(nn.Module):
     """
     Adding an identity allows us to keep things general in certain places.
     """

     def __init__(self):
         super(Identity, self).__init__()

     def forward(self, x):
         return x

class ResidualPhase(nn.Module):
    """
    Residual Genome phase.
    """

    def __init__(self, gene, in_channels, out_channels, idx, preact=False):
        """
        Constructor.
        :param gene: list, element of genome describing connections in this phase.
        :param in_channels: int, number of input channels.
        :param out_channels: int, number of output channels.
        :param idx: int, index in the network.
        :param preact: should we use the preactivation scheme?
        """
        super(ResidualPhase, self).__init__()

        #--- Flag to tell us if we need to increase       channel size.
        self.channel_flag = in_channels != out_channels
        self.first_conv = nn.Conv2d(in_channels, out_channels, kernel_size=1 if idx != 0 else 3,        stride=1, bias=False)
        self.dependency_graph = ResidualPhase.build_dependency_graph(gene)

        if preact:
            node_constructor = PreactResidualNode
        else:
            node_constructor = ResidualNode

        nodes = []
        for i in range(len(gene)):
            if len(self.dependency_graph[i + 1]) > 0:
                nodes.append(node_constructor(out_channels, out_channels))
            else:
                #--- Module list will ignore NoneType.
                nodes.append(None)

        self.nodes = nn.ModuleList(nodes)

        #
        # At this point, we know which nodes will be receiving input from where.
        # So, we build the 1x1 convolutions that will deal with the depth-wise concatenations.
        #
        conv1x1s = [Identity()] + [Identity() for _ in range(max(self.dependency_graph.keys()))]
        for node_idx, dependencies in self.dependency_graph.items():
            if len(dependencies) > 1:
                conv1x1s[node_idx] = \
                    nn.Conv2d(len(dependencies) * out_channels, out_channels, kernel_size=1,            bias=False)

        self.processors = nn.ModuleList(conv1x1s)
        self.out = nn.Sequential(
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    @staticmethod
    def build_dependency_graph(gene):
        """
        Build a graph describing the connections of a phase.
        "Repairs" made are as follows:
            - If a node has no input, but gives output, connect it to the input node (index 0 in        outputs).
            - If a node has input, but no output, connect it to the output node (value returned from    forward method).
        :param gene: gene describing the phase connections.
        :return: dict
        """
        graph = {}
        residual = gene[-1][0] == 1

        #--- First pass, build the graph without repairs.
        graph[1] = []
        for i in range(len(gene) - 1):
            graph[i + 2] = [j + 1 for j in range(len(gene[i])) if gene[i][j] == 1]

        graph[len(gene) + 1] = [0] if residual else []

        #--- Determine which nodes, if any, have no inputs and/or outputs.
        no_inputs = []
        no_outputs = []
        for i in range(1, len(gene) + 1):
            if len(graph[i]) == 0:
                no_inputs.append(i)

            has_output = False
            for j in range(i + 1, len(gene) + 2):
                if i in graph[j]:
                    has_output = True
                    break

            if not has_output:
                no_outputs.append(i)

        for node in no_outputs:
            if node not in no_inputs:
                #--- No outputs, but has inputs. Connect to output node.
                graph[len(gene) + 1].append(node)

        for node in no_inputs:
            if node not in no_outputs:
                #--- No inputs, but has outputs. Connect to input node.
                graph[node].append(0)

        return graph

    def forward(self, x):
        if self.channel_flag:
            x = self.first_conv(x)

        outputs = [x]

        for i in range(1, len(self.nodes) + 1):
            if not self.dependency_graph[i]:  # Empty list, no outputs to give.
                outputs.append(None)
            else:
                outputs.append(self.nodes[i - 1](self.process_dependencies(i, outputs)))

        return self.out(self.process_dependencies(len(self.nodes) + 1, outputs))

    def process_dependencies(self, node_idx, outputs):
        """
        Process dependencies with a depth-wise concatenation and
        :param node_idx: int,
        :param outputs: list, current outputs
        :return: Variable
        """
        return self.processors[node_idx](torch.cat([outputs[i] for i in self.                           dependency_graph[node_idx]], dim=1))

class ResidualGenomeDecoder(ChannelBasedDecoder):
     """
     Genetic CNN genome decoder with residual bit.
     """

     def __init__(self, list_genome, channels, preact=False, repeats=None):
         """
         Constructor.
         :param list_genome: list, genome describing the connections in a network.
         :param channels: list, list of tuples describing the channel size changes.
         :param repeats: None | list, list of integers describing how many times to repeat each phase.
         """
         super().__init__(list_genome, channels, repeats=repeats)

         #--- Exit if the parent constructor set the model.
         if self._model is not None:
             return

         #--- Build up the appropriate number of phases.
         phases = []
         for idx, (gene, (in_channels, out_channels)) in enumerate(zip(self._genome, self._channels)):
             phases.append(ResidualPhase(gene, in_channels, out_channels, idx, preact=preact))

         self._model = nn.Sequential(*self.build_layers(phases))

     def get_model(self):
         """
         :return: nn.Module
         """
         return self._model


class MacroNetwork(nn.Module):
    """
    Entire network.
    Made up of Phases.
    """
    def __init__(self, genome, channels, out_features, data_shape, decoder="residual", repeats=None):
        """
        Network constructor.
        :param genome: depends on decoder scheme, for most this is a list.
        :param channels: list of desired channel tuples.
        :param out_features: number of output features.
        :param decoder: string, what kind of decoding scheme to use.
        """
        super(MacroNetwork, self).__init__()

        assert len(channels) == len(genome), "Need to supply as many channel tuples as genes."
        if repeats is not None:
            assert len(repeats) == len(genome), "Need to supply repetition information for each phase."

        self.model = ResidualGenomeDecoder(genome, channels, repeats=repeats).get_model()

        #--- the flop count information is sotored
        self.start_flops_count = None
        self.stop_flops_count = None
        self.reset_flops_count = None
        self.compute_average_flops_cost = None

        #--- After the evolved part of the network,
        #--- we would like to do global average pooling and a linear layer.
        #--- However, we don't know the output size
        #--- so we do some forward passes and observe the output sizes.

        out = self.model(torch.autograd.Variable(torch.zeros(1, channels[0][0], *data_shape)))
        shape = out.data.shape

        self.gap = nn.AvgPool2d(kernel_size=(shape[-2], shape[-1]), stride=1)

        shape = self.gap(out).data.shape

        self.linear = nn.Linear(shape[1] * shape[2] * shape[3], out_features)

        #--- We accumulated some unwanted gradient information data with those forward passes.
        self.model.zero_grad()

    def forward(self, x):
        """
        Forward propagation.
        :param x: Variable, input to network.
        :return: Variable.
        """
        x = self.gap(self.model(x))

        x = x.view(x.size(0), -1)

        return self.linear(x), None

    def warmstart_weights(self,table_of_runs):
        """
        As of now support only reading params exactly from other run
        """

        #--- Just take the first row regardless which is the best accuracy model so far
        save_path = table_of_runs.iloc[[0]]['save_path'].item()
        if not os.path.isfile(save_path):
            logger.error(f"ERROR cannot open file {save_path} for weight warm start")
            return

        pretrained_model = torch.load(save_path)
        pretrained_dict = pretrained_model.state_dict()

        # 1. filter out unnecessary keys
        #pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in self.state_dict()}
        #--- Looks like batch norm add an extra layer, so we can not just replace
        for name, param in pretrained_dict.items():
            if name not in self.state_dict():
                continue
            if isinstance(param, nn.Parameter):
                #--- backwards compatibility for serialized parameters
                param = param.data

            #--- Make sure weight shape is the same
            if param.shape  != self.state_dict()[name].shape:
                continue

            self.state_dict()[name].copy_(param)

        # 2. overwrite entries in the existing state dict
        self.state_dict().update(self.state_dict())
        # 3. load the new state dict
        self.load_state_dict(self.state_dict())

#############################################################################################
#                            SECTION 3: Micro Genetic Network
#############################################################################################
Genotype = namedtuple('Genotype', 'normal normal_concat reduce reduce_concat')
PRIMITIVES = [
     'max_pool_3x3',
     'avg_pool_3x3',
     'skip_connect',
     'sep_conv_3x3',
     'sep_conv_5x5',
     'dil_conv_3x3',
     'dil_conv_5x5',
     'sep_conv_7x7',
     'conv_7x1_1x7',
 ]

OPS = {
    'none': lambda C, stride, affine: Zero(stride),
    'avg_pool_3x3': lambda C, stride, affine: nn.AvgPool2d(3, stride=stride, padding=1,                  count_include_pad=False),
    'max_pool_3x3': lambda C, stride, affine: nn.MaxPool2d(3, stride=stride, padding=1),
    'skip_connect': lambda C, stride, affine: Identity() if stride == 1 else FactorizedReduce(C, C,      affine=affine),
    'sep_conv_3x3': lambda C, stride, affine: SepConv(C, C, 3, stride, 1, affine=affine),
    'sep_conv_5x5': lambda C, stride, affine: SepConv(C, C, 5, stride, 2, affine=affine),
    'sep_conv_7x7': lambda C, stride, affine: SepConv(C, C, 7, stride, 3, affine=affine),
    'dil_conv_3x3': lambda C, stride, affine: DilConv(C, C, 3, stride, 2, 2, affine=affine),
    'dil_conv_5x5': lambda C, stride, affine: DilConv(C, C, 5, stride, 4, 2, affine=affine),
    'conv_7x1_1x7': lambda C, stride, affine: nn.Sequential(
        nn.ReLU(inplace=False),
        nn.Conv2d(C, C, (1, 7), stride=(1, stride), padding=(0, 3), bias=False),
        nn.Conv2d(C, C, (7, 1), stride=(stride, 1), padding=(3, 0), bias=False),
        nn.BatchNorm2d(C, affine=affine)
    ),
}


class ReLUConvBN(nn.Module):

    def __init__(self, C_in, C_out, kernel_size, stride, padding, affine=True):
        super(ReLUConvBN, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_out, kernel_size, stride=stride, padding=padding, bias=False),
            nn.BatchNorm2d(C_out, affine=affine)
        )

    def forward(self, x):
        return self.op(x)



class DilConv(nn.Module):

    def __init__(self, C_in, C_out, kernel_size, stride, padding, dilation, affine=True):
        super(DilConv, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=stride, padding=padding,               dilation=dilation,
                      groups=C_in, bias=False),
            nn.Conv2d(C_in, C_out, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_out, affine=affine),
        )

    def forward(self, x):
        return self.op(x)


class SepConv(nn.Module):

    def __init__(self, C_in, C_out, kernel_size, stride, padding, affine=True):
        super(SepConv, self).__init__()
        self.op = nn.Sequential(
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=stride, padding=padding, groups=C_in,  bias=False),
            nn.Conv2d(C_in, C_in, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_in, affine=affine),
            nn.ReLU(inplace=False),
            nn.Conv2d(C_in, C_in, kernel_size=kernel_size, stride=1, padding=padding, groups=C_in,       bias=False),
            nn.Conv2d(C_in, C_out, kernel_size=1, padding=0, bias=False),
            nn.BatchNorm2d(C_out, affine=affine),
        )

    def forward(self, x):
        return self.op(x)

class Zero(nn.Module):

    def __init__(self, stride):
        super(Zero, self).__init__()
        self.stride = stride

    def forward(self, x):
        if self.stride == 1:
            return x.mul(0.)
        return x[:, :, ::self.stride, ::self.stride].mul(0.)


class FactorizedReduce(nn.Module):

    def __init__(self, C_in, C_out, affine=True):
        super(FactorizedReduce, self).__init__()
        assert C_out % 2 == 0
        self.relu = nn.ReLU(inplace=False)
        self.conv_1 = nn.Conv2d(C_in, C_out // 2, 1, stride=2, padding=0, bias=False)
        self.conv_2 = nn.Conv2d(C_in, C_out // 2, 1, stride=2, padding=0, bias=False)
        self.bn = nn.BatchNorm2d(C_out, affine=affine)

    def forward(self, x):
        x = self.relu(x)
        out = torch.cat([self.conv_1(x), self.conv_2(x[:, :, 1:, 1:])], dim=1)
        out = self.bn(out)
        return out

class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y.expand_as(x)



class Cell(nn.Module):

    def __init__(self, genotype, C_prev_prev, C_prev, C, reduction, reduction_prev, SE=False):
        super(Cell, self).__init__()

        self.se_layer = None

        if reduction_prev:
            self.preprocess0 = FactorizedReduce(C_prev_prev, C)
        else:
            self.preprocess0 = ReLUConvBN(C_prev_prev, C, 1, 1, 0)
        self.preprocess1 = ReLUConvBN(C_prev, C, 1, 1, 0)

        if reduction:
            op_names, indices = zip(*genotype.reduce)
            concat = genotype.reduce_concat
        else:
            op_names, indices = zip(*genotype.normal)
            concat = genotype.normal_concat
        self._compile(C, op_names, indices, concat, reduction)

        if SE:
            self.se_layer = SELayer(channel=self.multiplier * C)

    def _compile(self, C, op_names, indices, concat, reduction):
        assert len(op_names) == len(indices)
        self._steps = len(op_names) // 2
        self._concat = concat
        self.multiplier = len(concat)

        self._ops = nn.ModuleList()
        for name, index in zip(op_names, indices):
            stride = 2 if reduction and index < 2 else 1
            op = OPS[name](C, stride, True)
            self._ops += [op]
        self._indices = indices

    def forward(self, s0, s1, drop_prob):
        s0 = self.preprocess0(s0)
        s1 = self.preprocess1(s1)

        states = [s0, s1]
        for i in range(self._steps):
            h1 = states[self._indices[2 * i]]
            h2 = states[self._indices[2 * i + 1]]
            op1 = self._ops[2 * i]
            op2 = self._ops[2 * i + 1]
            h1 = op1(h1)
            h2 = op2(h2)
            if self.training and drop_prob > 0.:
                if not isinstance(op1, Identity):
                    h1 = drop_path(h1, drop_prob)
                if not isinstance(op2, Identity):
                    h2 = drop_path(h2, drop_prob)
            s = h1 + h2
            states += [s]

        if self.se_layer is None:
            return torch.cat([states[i] for i in self._concat], dim=1)
        else:
            return self.se_layer(torch.cat([states[i] for i in self._concat], dim=1))


class MicroNetwork(nn.Module):

    def __init__(self, C, num_classes, layers, auxiliary, genotype, SE=False):
        super(MicroNetwork, self).__init__()
        self._layers = layers
        self._auxiliary = auxiliary
        self.in_channels = 1
        self.droprate = 0.0
        stem_multiplier = 3
        C_curr = stem_multiplier * C
        self.stem = nn.Sequential(
            nn.Conv2d(self.in_channels, C_curr, 3, padding=1, bias=False),
            nn.BatchNorm2d(C_curr)
        )

        C_prev_prev, C_prev, C_curr = C_curr, C_curr, C
        self.cells = nn.ModuleList()
        reduction_prev = False
        for i in range(layers):
            if i in [layers // 3, 2 * layers // 3]:
                C_curr *= 2
                reduction = True
            else:
                reduction = False

            cell = Cell(genotype, C_prev_prev, C_prev, C_curr, reduction, reduction_prev, SE=SE)

            reduction_prev = reduction
            self.cells += [cell]
            C_prev_prev, C_prev = C_prev, cell.multiplier * C_curr
            if i == 2 * layers // 3:
                C_to_auxiliary = C_prev

        if auxiliary:
            self.auxiliary_head = AuxiliaryHeadCIFAR(C_to_auxiliary, num_classes)
        self.global_pooling = nn.AdaptiveAvgPool2d(1)
        self.classifier = nn.Linear(C_prev, num_classes)

    def forward(self, input):
        logits_aux = None
        s0 = s1 = self.stem(input)
        for i, cell in enumerate(self.cells):
            s0, s1 = s1, cell(s0, s1, self.droprate)
            if i == 2 * self._layers // 3:
                if self._auxiliary and self.training:
                    logits_aux = self.auxiliary_head(s1)
        out = self.global_pooling(s1)
        logits = self.classifier(out.view(out.size(0), -1))
        return logits, logits_aux

    def warmstart_weights(self,table_of_runs):
        """
        As of now support only reading params exactly from other run
        """

        #--- Just take the first row regardless which is the best accuracy model so far
        save_path = table_of_runs.iloc[[0]]['save_path'].item()
        if not os.path.isfile(save_path):
            logger.error(f"ERROR cannot open file {save_path} for weight warm start")
            return

        pretrained_model = torch.load(save_path)
        pretrained_dict = pretrained_model.state_dict()

        # 1. filter out unnecessary keys
        #pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in self.state_dict()}
        #--- Looks like batch norm add an extra layer, so we can not just replace
        for name, param in pretrained_dict.items():
            if name not in self.state_dict():
                continue
            if isinstance(param, nn.Parameter):
                #--- backwards compatibility for serialized parameters
                param = param.data

            #--- Make sure weight shape is the same
            if param.shape  != self.state_dict()[name].shape:
                continue

            self.state_dict()[name].copy_(param)

        # 2. overwrite entries in the existing state dict
        self.state_dict().update(self.state_dict())
        # 3. load the new state dict
        self.load_state_dict(self.state_dict())

