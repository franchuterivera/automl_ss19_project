import collections
import utils
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
import pandas as pd
import numpy as np
import sqlalchemy as sql
from sqlalchemy import Column, Integer, String, Table
import time
import logging
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
import pickle
import os
import glob
import json
import re, ast

#--- Create a custom logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARN)
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
formatter = logging.Formatter('[%(name)s,%(levelname)s]: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class CollectiveMemory:
    def __init__(self, memory_name="memory.db"):
        #--- SQL stuff
        connect_string = f"sqlite:///{memory_name}"
        self.tablename = "memory"
        self.engine = sql.create_engine(connect_string)

        #--- Learning curve prediction
        self.min_datapoints_lc_prediction = 40
        self.min_datapoints_lc_prediction = 5
        self.lc_predictor_path = 'lc_predictor'
        self.lc_pred_model = {}
        self.load_lc_predictor()

        #--- Mostly we would be working with a pandas object
        self.basic_stadistics = [
            "seed", "start", "end", "method", "budget",
            "accuracy",  "accuracy_history", "save_path", "NAS"
        ]
        self.settings_config = [
            "activation", "batch_size", "lr",
            "optimizer", "sgd_momentum", "rms_momentum",
            "weight_decay", "beta0", "beta1", "rho" , "alpha",
            "scheduler", "train_dataset",
        ]
        self.preprocessing = [
            "GaussianBlur",
            "CV2threshold",
            "medianBlur",
            "RandomRotation",
        ]
        self.NSGA_arch_config = ["genome","init_channels", "layers"]
        self.BOHB_arch_config = [
            "duplications", "skip_connection",
            "num_conv_layers", "kernel_size", "dropout",
            "num_filters_0", "num_filters_1", "num_filters_2",
            "BatchNorm2d", "BatchNorm1d", "num_fc_layers",
            "num_fc_outputs_0", "num_fc_outputs_1", "num_fc_outputs_2",
            "bottleneck"
        ]
        self.arch_config = self.BOHB_arch_config + self.NSGA_arch_config
        self.config_space = self.settings_config + self.preprocessing + self.arch_config

        self.baseline_config = {
            "activation": "ReLU", "batch_size" : 96, "lr" : 0.01,
            "optimizer": "Adam", "sgd_momentum" : None, "rms_momentum" : None,
            "weight_decay": 0, "beta0" : 0.9, "beta1" : 0.999,
            "rho" : None, "alpha": None,
            "scheduler" : None, "train_dataset": 'K49', "duplications" : 0, "skip_connection":0,
            "num_conv_layers" : 1, "kernel_size" : 2, "dropout" : 0.2,
            "num_filters_0" : 4, "num_filters_1" : 0, "num_filters_2" : 0, "BatchNorm2d":0, "BatchNorm1d":0, "num_fc_layers":1,
            "num_fc_outputs_0":256,"num_fc_outputs_1":0,"num_fc_outputs_2":0, "bottleneck":0,
            "genome":None, "init_channels":None,  "layers":None, "NAS":'BOHB'
        }

        self.highest_score = 0.60

        self.memory = pd.DataFrame(columns= self.basic_stadistics + self.config_space)

        self.update()

    def correct_column_types(self):
        """Makes sure column have correct typing"""
        self.memory['accuracy'] = self.memory['accuracy'].apply(pd.to_numeric)
        self.memory['start'] = self.memory['start'].apply(pd.to_numeric)
        self.memory['end'] = self.memory['end'].apply(pd.to_numeric)


    def update(self):
        """Updates the memory from the database"""
        #--- If there is a table, then read the configs
        if self.engine.dialect.has_table(self.engine, self.tablename):
            self.memory = pd.read_sql_query(f"select * from {self.tablename}", self.engine)
            self.correct_column_types()
            self.highest_score = self.get_highest_score()

    def save(self):
        """Save your information to a database"""
        #--- wait for someone to stop writting to db
        counter = 0
        while self.engine.dialect.has_table(self.engine, "temporal"):
            time.sleep(2)
            counter +=1
            if counter >= 100:
                logger.error(f"ERROR: temporal DB locked for too long... killing lock!")
                break

        #--- save private info to common db
        try:
            self.memory.to_sql("temporal", con=self.engine, if_exists='replace')
            if not self.engine.dialect.has_table(self.engine, self.tablename):
                #--- First time ever writing
                self.memory.to_sql(self.tablename, con=self.engine)
            else:
                #--- combine both memories, ignore the index column
                db_memory = pd.read_sql_query(f"select * from {self.tablename}", self.engine)
                db_memory = db_memory.drop(columns=['index'])
                if 'index' in self.memory.columns:
                    self.memory = self.memory.drop(columns=['index'])
                self.memory = pd.concat([self.memory, db_memory]).drop_duplicates().reset_index(drop=True)
                self.memory.to_sql(self.tablename, con=self.engine, if_exists='replace')

            #--- Remove the temporal table to allow other process to write
            with self.engine.connect() as con:
                con.execute('DROP TABLE temporal')
        except Exception as e:
            logger.error(f"ERROR: Could not write to the database because {e}!")


    def addConfigMemory(self, config):
        """
        Takes a config and add it as a row to pandas
        import datetime;
        """
        self.memory = self.memory.append(config, ignore_index=True)


    def getConfigSpace(self, config_type: str = 'ALL'):
        """
        Shared configuration space among processes... just to have it in a single place!
        """
        cs = CS.ConfigurationSpace()

        #--- 'duplications'
        #cs.add_hyperparameters([
        #    CSH.UniformIntegerHyperparameter('duplications', lower=1, upper=3, default_value=1, log=False)
        #])

        #--- 'skip_connection'
        #cs.add_hyperparameters([
        #    CSH.CategoricalHyperparameter('skip_connection', [True, False])
        #])

        if config_type in ['ALL', 'NAS']:

            #--- 'num_conv_layers'
            num_conv_layers = CSH.UniformIntegerHyperparameter('num_conv_layers', lower=1, upper=3, default_value=2)
            num_filters_0 = CSH.UniformIntegerHyperparameter('num_filters_0', lower=4, upper=16, default_value=4)
            num_filters_1 = CSH.UniformIntegerHyperparameter('num_filters_1', lower=4, upper=16, default_value=4)
            num_filters_2 = CSH.UniformIntegerHyperparameter('num_filters_2', lower=4, upper=16, default_value=4)
            cs.add_hyperparameters([num_conv_layers, num_filters_0, num_filters_1, num_filters_2])
            cs.add_condition(CS.GreaterThanCondition(num_filters_1, num_conv_layers, 1))
            cs.add_condition(CS.GreaterThanCondition(num_filters_2, num_conv_layers, 2))

            #--- 'kernel_size'
            cs.add_hyperparameters([
                CSH.UniformIntegerHyperparameter('kernel_size', lower=2, upper=3, default_value=3)
            ])

            #--- 'dropout'
            cs.add_hyperparameters([
                CSH.UniformFloatHyperparameter('dropout', lower=0.0, upper=0.8, default_value=0.5)
            ])

            #--- 'BatchNorm2d'
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('BatchNorm2d', [True, False])
            ])

            #--- 'BatchNorm1d'
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('BatchNorm1d', [True, False])
            ])

            #--- 'num_fc_layers'
            num_fc_layers = CSH.UniformIntegerHyperparameter('num_fc_layers', lower=1, upper=3, default_value=2)
            num_fc_outputs_0 = CSH.UniformIntegerHyperparameter('num_fc_outputs_0', lower=64, upper=512, default_value=256)
            num_fc_outputs_1 = CSH.UniformIntegerHyperparameter('num_fc_outputs_1', lower=64, upper=512, default_value=256)
            num_fc_outputs_2 = CSH.UniformIntegerHyperparameter('num_fc_outputs_2', lower=64, upper=512, default_value=256)
            cs.add_hyperparameters([num_fc_layers, num_fc_outputs_0, num_fc_outputs_1, num_fc_outputs_2])
            cs.add_condition(CS.GreaterThanCondition(num_fc_outputs_1, num_fc_layers, 1))
            cs.add_condition(CS.GreaterThanCondition(num_fc_outputs_2, num_fc_layers, 2))

            #--- 'bottleneck'
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('bottleneck', [True, False])
            ])

            #--- 'activation'
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('activation', ['ReLU', 'LeakyReLU', 'Tanh'])
            ])

        #--- 'batch_size'
        if config_type in ['ALL', 'HPO']:
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('batch_size', [64,128,256])
            ])

            #--- 'lr'
            cs.add_hyperparameters([
                CSH.UniformFloatHyperparameter('lr', lower=1e-6, upper=1e-1, default_value='1e-2', log=True)
            ])

            #--- 'optimizer'
            optimizer = CSH.CategoricalHyperparameter('optimizer', ['Adam', 'Adadelta', 'SGD', 'RMSprop'])
            cs.add_hyperparameters([optimizer])

            #--- 'momemtum'
            sgd_momentum = CSH.UniformFloatHyperparameter('sgd_momentum', lower=0.0, upper=0.99, default_value=0.9)
            rms_momentum = CSH.UniformFloatHyperparameter('rms_momentum', lower=0.0, upper=0.99, default_value=0.9)
            cs.add_hyperparameters([sgd_momentum, rms_momentum])
            cs.add_condition(CS.EqualsCondition(sgd_momentum, optimizer, 'SGD'))
            cs.add_condition(CS.EqualsCondition(rms_momentum, optimizer, 'RMSprop'))

            #--- 'weight_decay'
            cs.add_hyperparameters([
                CSH.UniformFloatHyperparameter('weight_decay', lower=0.0, upper=0.9, default_value=0.0)
            ])

            #--- 'beta0'
            beta0 = CSH.UniformFloatHyperparameter('beta0', lower=0.0, upper=0.99, default_value=0.9)
            beta1 = CSH.UniformFloatHyperparameter('beta1', lower=0.0, upper=0.999, default_value=0.999)
            cs.add_hyperparameters([beta0, beta1])
            cs.add_condition(CS.EqualsCondition(beta0, optimizer, 'Adam'))
            cs.add_condition(CS.EqualsCondition(beta1, optimizer, 'Adam'))

            #--- 'rho'
            rho = CSH.UniformFloatHyperparameter('rho', lower=0.0, upper=0.9, default_value=0.9)
            cs.add_hyperparameters([rho])
            cs.add_condition(CS.EqualsCondition(rho, optimizer, 'Adadelta'))

            #--- 'alpha'
            alpha = CSH.UniformFloatHyperparameter('alpha', lower=0.0, upper=0.99, default_value=0.99)
            cs.add_hyperparameters([alpha])
            cs.add_condition(CS.EqualsCondition(alpha, optimizer, 'RMSprop'))

            #--- 'scheduler'
            cs.add_hyperparameters([
                CSH.CategoricalHyperparameter('scheduler', ['StepLR', 'ExponentialLR', 'CosineAnnealingLR'])
            ])

        #--- Try to learn which dataset is good for learning
        #cs.add_hyperparameters([
        #    CSH.CategoricalHyperparameter('train_dataset', ['full', 'small', 'blurred_small', 'segmented_small'])
        #])

        if config_type in ['ALL', 'PREPROCESSING']:
            for transform in self.preprocessing:
                cs.add_hyperparameters([
                    CSH.CategoricalHyperparameter(transform, [True, False])
                ])

        return cs

    def prune_config(self, dictionary):
        """
        Removes not supported keys from dictionary
        """
        keys_to_delete = []
        for key in dictionary.keys():
            if key not in self.config_space:
                keys_to_delete.append(key)
            if isinstance(dictionary[key], np.int64):
                dictionary[key] = int(dictionary[key])
        for key in keys_to_delete:
            del dictionary[key]
        return dictionary
    def is_genome_similar(self, a, b):
        """Returns true if two genomes have at least one gene in common"""

        #--- handle none case
        if not utils.is_valid(a) and utils.is_valid(b):
            return True
        if not utils.is_valid(a) or not utils.is_valid(b):
            return False

        #genome_a = pickle.loads(a)
        #genome_b = pickle.loads(b)
        genome_a = np.array(ast.literal_eval(a))
        genome_b = np.array(ast.literal_eval(b))

        similarity = 0
        for i in range(len(genome_a)):
            for j in range(1,len(genome_a[i])):
                #--- Ignore the first element of gen, as its the join
                if np.array(genome_a[i][j]).tostring() != np.array(genome_b[i][j]).tostring() and similarity == 0:
                    return False
                similarity +=1
        return similarity


    def get_runs_by_config(self,config, filters=[]):
        """Filters the panda memory object to return desired configs"""

        return_object = pd.DataFrame(columns= self.basic_stadistics + self.config_space)
        for memory_to_dict in self.memory.to_dict('records'):
            #--- if all items of compare_dict are in memory_to_dict this is a valid config
            same = True
            for key in config.keys():

                #--- We want to reuse this utility to find warm start candidates
                #--- For genome, which might not be the same. If at least some
                #--- genes are similar, we can use it
                if key is 'genome':
                    if 'NAS' in memory_to_dict and 'NAS' in config and memory_to_dict["NAS"] == config["NAS"]:
                        if self.is_genome_similar(memory_to_dict[key], config[key]):
                            continue

                #--- if the key is not found in any of the dicts, assume false to be safe
                if key not in memory_to_dict.keys() or  memory_to_dict[key] != config[key]:
                    same=False

            if same: return_object = return_object.append(memory_to_dict,ignore_index=True)

        #--- handle filters, I am lazy so just one at a time
        for column in filters:
            return_object = return_object[pd.notnull(return_object[column])]

        #--- Nothing more to do on a empty object
        if return_object.size == 0:
            return return_object

        #--- Sort by accuracy!
        return return_object.sort_values(by=['accuracy'])

    def get_highest_score(self):
        """
        Returns the maximum score seen so far
        """
        if self.memory.empty:
            return 0
        return self.memory['accuracy'].max()

    def get_best_run_dict(self):
        """Returns the config of the best run"""
        if self.memory.empty or self.memory.shape[0] == 0:
            return None
        self.memory['accuracy'] = self.memory['accuracy'].apply(pd.to_numeric)
        return self.memory.to_dict('records')[self.memory['accuracy'].idxmax()]

    def get_architecture_from_config(self, config):
        """Returns the architectural config from a given configuration dict"""

        if 'NAS' not in config: return None

        return config['NAS']

    def complete_config(self, config):
        """
        Completes a config with the best run seen so far
        """

        #--- The one thing we cannot mess up is having convined
        #--- Component information. That is, if the config already
        #--- contains genome, dont add BOHB config arch info
        arch_type = self.get_architecture_from_config(config)
        architectures = self.arch_config
        if arch_type is not None and 'BOHB' in arch_type: architectures = self.BOHB_arch_config
        if arch_type is not None and 'NSGA' in arch_type: architectures = self.NSGA_arch_config

        best_run_config = self.get_best_run_dict()
        if best_run_config is None:
            best_run_config = self.baseline_config

        for necessary_item in self.settings_config + architectures:
            if necessary_item not in config and necessary_item in best_run_config:
                logger.debug(f"Completed the missing config {necessary_item}  with {best_run_config[necessary_item]}")
                config[necessary_item] = best_run_config[necessary_item]

        #--- If no arch as of this point, it means it comes from baseline
        if 'NAS' not in config: config['NAS'] = best_run_config['NAS']

        return config

    def get_biggest_valid_datapoints(self,accuracy_history):
        """Returns the biggest valid datapoints to build a learning curve pred"""
        valid_rows = np.count_nonzero(~np.isnan(accuracy_history), axis=1)

        epochs_of_history = accuracy_history.shape[1]

        while epochs_of_history > 0:
            number_valid_histories = np.count_nonzero(valid_rows>=epochs_of_history)

            if number_valid_histories > self.min_datapoints_lc_prediction:
                valid_rows = np.argwhere(valid_rows>=epochs_of_history)
                break
            epochs_of_history -=1

        return valid_rows.squeeze(), epochs_of_history

    def get_run_history_data(self):
        """Gets the run history data to fit a prediction model"""

        #--- get the scores
        self.memory['accuracy'] = self.memory['accuracy'].apply(pd.to_numeric)
        scores = self.memory['accuracy'].values
        if not len(scores):
            return None, None

        #--- Get the accuracy_history
        accuracy_history = [lista[:-1].split(',') for lista in self.memory['accuracy_history'].tolist()]

        #--- Make every element float for convinience
        for i in range(len(accuracy_history)):
            for j in range(len(accuracy_history[i])):
                if isinstance(accuracy_history[i][j], float): continue
                if isinstance(accuracy_history[i][j], str) and not accuracy_history[i][j].replace('.','',1).isdigit():
                    accuracy_history[i][j] = np.nan
                else:
                    accuracy_history[i][j] = float(accuracy_history[i][j])

        length = max(map(len, accuracy_history))
        accuracy_history = np.array([xi+[np.nan]*(length-len(xi)) for xi in accuracy_history])

        #--- Preserve only usefull runs
        mask = np.count_nonzero(np.isnan(accuracy_history), axis=1)!=accuracy_history.shape[1]
        scores = scores[mask]
        accuracy_history = accuracy_history[mask]

        #--- Check if we have enough data to build the model.
        #--- At least 4 epochs to build the model
        row_mask, max_col_index = self.get_biggest_valid_datapoints(accuracy_history)
        if row_mask.shape[0] < self.min_datapoints_lc_prediction or max_col_index < 4:
            return None, None
        scores = scores[row_mask]
        accuracy_history = accuracy_history[row_mask][:,:max_col_index]

        #--- To create training data we assume 40% smaller runs are bad and 60% greater are good
        idx = np.argsort(scores)
        scores = scores[idx]
        accuracy_history = accuracy_history[idx]

        #-- argsort is descending order. Best runs at the end
        scores[:int(0.4*scores.shape[0])] = 0
        scores[int(0.6*scores.shape[0]):] = 1
        mask = np.ones(len(scores), dtype=bool)
        mask[int(0.4*scores.shape[0]):int(0.6*scores.shape[0])+1] = False
        scores = scores[mask,...]
        accuracy_history = accuracy_history[mask,...]

        return accuracy_history, scores

    def load_lc_predictor(self):
        """Loads learning curve predictor models if any"""
        self.lc_pred_model = {}
        if not os.path.exists(self.lc_predictor_path):
            os.makedirs(self.lc_predictor_path)
        models = glob.glob(self.lc_predictor_path + "/*pkl")
        if len(models) > 0:
            for model_path in models:
                logging.info(f"Loading LC model from {self.lc_predictor_path}...")
                epoch, ext = os.path.splitext(os.path.basename(model_path))
                self.lc_pred_model[int(epoch)] = pickle.load(open(model_path, 'rb'))

    def build_lc_predictor(self):
        """Builds a random forest predictor to see if
        it make sense to continue the run"""

        #--- First check if there are any pre-existing models
        self.load_lc_predictor()

        #--- Then see if we can proactively create more model. 4 is a nice number
        x, y = self.get_run_history_data()

        #--- We require a minimum number of datapoints to make sense
        if x is None or len(x) < self.min_datapoints_lc_prediction:
            return

        for epoch in range(4, x.shape[1] + 1, 4):

            #--- don't recreate existing models
            if epoch in self.lc_pred_model:
                continue

            #--- If there is not enough data, we cannot do anything
            this_x = x[:,:epoch]
            this_x, y = shuffle(this_x,y)
            X_train, X_test, y_train, y_test = train_test_split(this_x,y, test_size=0.3)
            self.lc_pred_model[epoch] = RandomForestClassifier(n_estimators=100)
            self.lc_pred_model[epoch].fit(X_train,y_train)
            y_pred=self.lc_pred_model[epoch].predict(X_test)
            if metrics.accuracy_score(y_test, y_pred) < 0.7:
                del self.lc_pred_model[epoch]
                continue
            else:
                print(f"Built a model for LC prediction  on {self.lc_predictor_path} with accuracy: {metrics.accuracy_score(y_test, y_pred)}")
                #--- save the model
                pickle.dump(self.lc_pred_model[epoch], open(self.lc_predictor_path+f"/{epoch}.pkl", 'wb'))

        return True
