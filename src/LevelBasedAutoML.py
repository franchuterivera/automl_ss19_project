from CollectiveMemory import CollectiveMemory
from BOHBComponent import BOHBComponent
from NSGAComponent import NSGAComponent
from BaseTorchCNNClass import print_fn
from multiprocessing import Process, Queue, Pipe
import collections
import torch
import copy
import numpy as np
from tqdm import tqdm
import time
import os.path
import sys

#--- Handling logging
import logging
logging.getLogger('hpbandster').setLevel(logging.WARN)
logging.getLogger('CollectiveMemory').setLevel(logging.WARN)
logging.getLogger('BOHBComponent').setLevel(logging.WARN)
logging.getLogger('BaseTorchCNNClass').setLevel(logging.WARN)
logging.getLogger('NSGAComponent').setLevel(logging.WARN)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(name)s,%(levelname)s]: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class DUMMY(Process):
    def __init__(
        self,
        run_id,
        config_type,
        iterations,
        dataset,
        dataset_reduction,
        seed,
        max_budget,
        queue,
        verbose,
        overwrite,
    ):
        super(DUMMY, self).__init__()
        self.run_id = run_id
        self.iterations = iterations
        self.queue = queue
        self.iteration = 0
        self.filename = self.run_id + ".log"
        self.max_budget = max_budget
        self.config_type = config_type

    def get_iteration(self):
        if not os.path.isfile(self.filename):
            return 0
        with open(self.filename) as the_file:
            content = the_file.readlines()
        return int(content[0])

    def run(self):
        while True:
            self.iteration += 1
            with open(self.filename, 'w') as the_file:
                the_file.write(str(self.iteration))
            if self.iteration>self.iterations:
                break
            time.sleep(self.config_type)
        if torch.cuda.is_available():
            self.queue.put(f"The process {self.run_id} ran in {torch.cuda.device(0)} in thread {self.pid}")
        else:
            self.queue.put(f"The process {self.run_id} ran in cpu with {torch.get_num_threads()} in thread {self.pid}")
        return

class Plan(object):
    """
    An object that registers the restriction of the level,
    in a pyramid of restricted resources
    """
    def __init__(self,steps):
        self.steps = steps
        self.tasks = collections.defaultdict(dict)

        for step in range(self.steps):
            self.tasks[step] = []

    def add_task(self,level,task):
        """Adds a task to run"""
        self.tasks[level].append(task)

    def get_count_processes(self, step=None):
        """Utility to count the number of tasks from this plan"""

        if step:
            return len(self.tasks[step])
        else:
            count = 0
            for key in self.tasks.keys():
                count += len(self.tasks[step])
        return count

#https://stackoverflow.com/questions/32193935/how-to-output-progress-of-parallel-process-on-regular-basis
#sub classed progress counter

class LevelBasedAutoML():
    """
    This class build a hierarchical/pyramidal scheduler of jobs.
    The justification for such complexity is to launch many exploration jobs
    at a very low cost, i.e.:
        smaller dataset
        smaller resolution
        maximum epochs of 5
    Which the lower level of information, we find which component
    has merit to run more jobs next time. The components currently supported:
        NAS_BOHB
        HPO_BOHB
        PRE_BOHB
    INPUTS:
        target_budget = max budget allowed overall
        min_budget = minimum budget a run can take
        steps = [...] a list indicating the how many runs per level
    """
    def __init__(
        self,
        target_budget : int,
        min_budget : int,
        steps: list,
        seed: int = 56,
    ):

        self.target_budget = target_budget
        self.min_budget = min_budget
        self.steps = steps
        self.seed = seed
        self.queue = Queue()
        self.number_of_process = 2
        self.iterations = 1
        self.processes = list()
        self.recv, self.sendr = Pipe() # pipe for example function to send progress
        self.current_size = 0
        self.memory = CollectiveMemory()

        #--- Iterations are a factor of the steps per level
        self.hpo_components = [
            {
                'name' :'HPO_BOHB',
                'object': BOHBComponent,
                'overwrite' : {
                    'config_type' : 'HPO',
                },
                'merit': 1,
            },
            {
                'name' :'HPO_PREPROCESSING',
                'object': BOHBComponent,
                'overwrite' : {
                    'config_type' : 'PREPROCESSING',
                },
                'merit': 0,
            },
        ]
        self.nas_components = [
            {
                'name' :'NAS_BOHB',
                'object': BOHBComponent,
                'overwrite' : {
                    'config_type' : 'NAS',
                },
                'merit': 0,
            },
            {
                'name' :'NAS_NSGA_macro',
                'object': NSGAComponent,
                'overwrite' : {
                    'config_type' : 'NSGA_macro',
                },
                'merit': 1,
            },
            {
                'name' :'NAS_NSGA_micro',
                'object': NSGAComponent,
                'overwrite' : {
                    'config_type' : 'NSGA_micro',
                },
                'merit': 1,
            },
        ]

    def run(self):
        """
        Runs a created plan. Anytime performance is guaranteed through the database
        which is lively wrote by the components
        """
        logger.info(f"Started running at {time.strftime('%l:%M%p %Z on %b %d, %Y') } a total of {len(self.steps)} levels...")
        logger.info(f"Anytime performance can be checked in memory.db in running path...")
        print("\n")

        ## Create a list to hold running Processor object instances...
        for step in range(len(self.steps)):
            print("\n")
            print("#"*40)
            print(f"\t\tLevel {step} / {len(self.steps)}")
            print("#"*40)

            for component in self.plan.tasks[step]:
                component.start()

                #--- run serially for now
                time.sleep(5)
                self.track_run_status(step, [component])
                component.join()

            #--- Wait for the jobs to finish
            #    https://stackoverflow.com/a/42137966/667301
            #self.track_run_status(step, self.plan.tasks[step])
            #[proc.join() for proc in self.plan.tasks[step]]

            #--- Nice printing
            print("\n")
            print("#"*40)
            print("\n")

            #--- check at the end of every step if we can builld
            #--- a lc predictor -- more like a classifier
            self.memory.update()
            self.memory.build_lc_predictor()

        logger.info(f"Completed running {len(self.steps)} levels...")

        #--- Print information about the best run found so far
        print_fn()
        logger.info(f"Finished at {time.strftime('%l:%M%p %Z on %b %d, %Y') } a total of {len(self.steps)} levels...")

    def track_run_status(self, step, jobs):
        """Tracks running jobs and print progress to STDOUT"""

        pbar_list = [tqdm(total=component.iterations, file=sys.stdout) for component in jobs]

        while np.any([component.is_alive() for component in jobs]):

            #--- See if we need to update the progress of the components
            for i, component in enumerate(jobs):
                new_progress = component.get_iteration()
                pbar_list[i].set_description(f"{component.run_id}")
                pbar_list[i].update(max(0,new_progress-pbar_list[i].n))
                pbar_list[i].refresh()

            #--- Checking everytime is an overkill
            time.sleep(1)

        #--- Close the tracking bars
        for i, component in enumerate(jobs):
            if pbar_list[i].n < component.iterations:
                pbar_list[i].update(component.iterations-pbar_list[i].n)
            pbar_list[i].close()

    def create_run_plan(self):
        """
        Based on the number of steps of the pyramid, create a
        running plan
        """

        self.plan = Plan(len(self.steps))

        #--- The first run set we want to do is with small requirements
        #--- Exploration in the domain of architecture with hyperparams
        #--- From the baseline which are the best available so far
        #--- (dataset|resolution|KMNIST)

        for i in range(len(self.steps)):

            #---Components depend on the required type
            if self.steps[i]['type'] == "NAS":
                components = self.nas_components
            elif self.steps[i]['type'] == "HPO":
                components = self.hpo_components
            else:
                raise Exception(f"Unsuported component type={self.steps[i]['type']}")

            for component in components:

                #--- Need to handle merit run here
                if not component['merit']:
                    continue

                #--- Reduce the resources of this components depending on the
                #--- level of the run
                this_component = component['object'](
                    run_id = f"{component['name']}_L{i}",
                    config_type = component['overwrite']['config_type'],
                    iterations = self.steps[i]['iterations'],
                    dataset = self.steps[i]['dataset'],
                    dataset_reduction = self.steps[i]['dataset_reduction'],
                    seed = self.seed,
                    max_budget = self.steps[i]['max_budget'],
                    queue = self.queue,
                    overwrite = True,
                )

                self.plan.add_task(level = i,task = this_component)

        logger.info(f"Created a plan to run {np.sum([len(tasks) for tasks in self.plan.tasks.values()])} tasks:{list(self.plan.tasks.items())}...")

    def merit_based_resource_alloc():
        """
        Allocate the runs allowed in this level to the component
        that yielded the best improvement so far
        """

    def train_learning_rate_predictor():
        """
        Create an RNN based on the database of good and bad runs,
        that would predict when it makes sense to keep the run going
        """

if __name__ == "__main__":

    #--- For testing the code
    test_steps = [
      {'type':'NAS', 'dataset':'KMNIST', 'dataset_reduction':0.999, 'max_budget':5, 'iterations':1},
      {'type':'HPO', 'dataset':'KMNIST', 'dataset_reduction':0.999, 'max_budget':5, 'iterations':1},
      {'type':'NAS', 'dataset':'KMNIST', 'dataset_reduction':0.999, 'max_budget':5, 'iterations':1},
      {'type':'HPO', 'dataset':'KMNIST', 'dataset_reduction':0.999, 'max_budget':5, 'iterations':1},
    ]
    steps = [
      {'type':'NAS', 'dataset':'K49', 'dataset_reduction':0.7, 'max_budget':5, 'iterations':10},
      {'type':'HPO', 'dataset':'K49', 'dataset_reduction':0.5, 'max_budget':5, 'iterations':12},
      {'type':'NAS', 'dataset':'K49', 'dataset_reduction':0.4, 'max_budget':10, 'iterations':3},
      {'type':'HPO', 'dataset':'K49', 'dataset_reduction':0,   'max_budget':20, 'iterations':8},
    ]
    scheduler = LevelBasedAutoML(
        target_budget = 5,
        min_budget = 1,
        #steps = test_steps,
        steps = steps,
        seed = -1,
    )

    scheduler.create_run_plan()
    scheduler.run()
