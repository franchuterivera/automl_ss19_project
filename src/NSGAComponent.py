############################################################################################
#This file is taken from https://github.com/ianwhale/nsga-net
#@article{nsganet,
#  title={NSGA-NET: a multi-objective genetic algorithm for neural architecture search},
#  author={Lu, Zhichao and Whalen, Ian and Boddeti, Vishnu and Dhebar, Yashesh and Deb, Kalyanmoy and Goodman, Erik and  Banzhaf, Wolfgang},
#  booktitle={GECCO-2019},
#  year={2018}
#}
#
#A bunch of adaptations are made to improve it, and can be diff with above link. But again,
#the core of the code is taken from above authors and here I want to acknowledge this.
#https://pymoo.org/
############################################################################################
import sys
import os
import time
import calendar
import logging
import argparse
import shutil
from utils import *
import pickle
import numpy as np
import torch
import torch.nn as nn
import torch.utils
from torch.utils.data import DataLoader, Dataset
import torch.backends.cudnn as cudnn
import torchvision.transforms as transforms
from BaseTorchCNNClass import MacroNetwork, train_fn, eval_fn, ResidualPhase, Genotype, MicroNetwork
from multiprocessing import Process, Queue
from CollectiveMemory import CollectiveMemory
from torchsummary import summary
from datasets import KMNIST, K49

#---For visualization
from graphviz import Digraph

from pymop.problem import Problem
from pymoo.optimize import minimize
from pymoo.algorithms.genetic_algorithm import GeneticAlgorithm
from pymoo.docs import parse_doc_string
from pymoo.model.individual import Individual
from pymoo.model.survival import Survival
from pymoo.operators.crossover.point_crossover import PointCrossover
from pymoo.operators.mutation.polynomial_mutation import PolynomialMutation
from pymoo.operators.sampling.random_sampling import RandomSampling
from pymoo.operators.selection.tournament_selection import compare, TournamentSelection
from pymoo.util.display import disp_multi_objective
from pymoo.util.dominator import Dominator
from pymoo.util.non_dominated_sorting import NonDominatedSorting
from pymoo.util.randomized_argsort import randomized_argsort

############################################################################################
#                                   Engine
############################################################################################
class NSGANet(GeneticAlgorithm):

    def __init__(self, **kwargs):
        kwargs['individual'] = Individual(rank=np.inf, crowding=-1)
        super().__init__(**kwargs)

        self.tournament_type = 'comp_by_dom_and_crowding'
        self.func_display_attrs = disp_multi_objective

def binary_tournament(pop, P, algorithm, **kwargs):
    if P.shape[1] != 2:
        raise ValueError("Only implemented for binary tournament!")

    tournament_type = algorithm.tournament_type
    S = np.full(P.shape[0], np.nan)

    for i in range(P.shape[0]):

        a, b = P[i, 0], P[i, 1]

        #--- if at least one solution is infeasible
        if pop[a].CV > 0.0 or pop[b].CV > 0.0:
            S[i] = compare(
                a,
                pop[a].CV,
                b,
                pop[b].CV,
                method='smaller_is_better',
                return_random_if_equal=True
            )

        #--- both solutions are feasible
        else:

            if tournament_type == 'comp_by_dom_and_crowding':
                rel = Dominator.get_relation(pop[a].F, pop[b].F)
                if rel == 1:
                    S[i] = a
                elif rel == -1:
                    S[i] = b

            elif tournament_type == 'comp_by_rank_and_crowding':
                S[i] = compare(a, pop[a].rank, b, pop[b].rank,
                               method='smaller_is_better')

            else:
                raise Exception("Unknown tournament type.")

            #--- if rank or domination relation didn't make a decision compare by crowding
            if np.isnan(S[i]):
                S[i] = compare(a, pop[a].get("crowding"), b, pop[b].get("crowding"),
                               method='larger_is_better', return_random_if_equal=True)

    return S[:, None].astype(np.int)


class RankAndCrowdingSurvival(Survival):

    def __init__(self) -> None:
        super().__init__(True)

    def _do(self, pop, n_survive, D=None, **kwargs):

        #--- get the objective space values and objects
        F = pop.get("F")

        #--- the final indices of surviving individuals
        survivors = []

        #--- do the non-dominated sorting until splitting front
        fronts = NonDominatedSorting().do(F, n_stop_if_ranked=n_survive)

        for k, front in enumerate(fronts):

            #--- calculate the crowding distance of the front
            crowding_of_front = calc_crowding_distance(F[front, :])

            #--- save rank and crowding in the individual class
            for j, i in enumerate(front):
                pop[i].set("rank", k)
                pop[i].set("crowding", crowding_of_front[j])

            #--- current front sorted by crowding distance if splitting
            if len(survivors) + len(front) > n_survive:
                I = randomized_argsort(crowding_of_front, order='descending', method='numpy')
                I = I[:(n_survive - len(survivors))]

            #--- otherwise take the whole front unsorted
            else:
                I = np.arange(len(front))

            #--- extend the survivors by all or selected individuals
            survivors.extend(front[I])

        return pop[survivors]

def calc_crowding_distance(F):
    infinity = 1e+14

    n_points = F.shape[0]
    n_obj = F.shape[1]

    if n_points <= 2:
        return np.full(n_points, infinity)
    else:

        #--- sort each column and get index
        I = np.argsort(F, axis=0, kind='mergesort')

        #--- now really sort the whole array
        F = F[I, np.arange(n_obj)]

        #--- get the distance to the last element in sorted list and replace zeros with actual values
        dist = np.concatenate([F, np.full((1, n_obj), np.inf)]) \
               - np.concatenate([np.full((1, n_obj), -np.inf), F])

        index_dist_is_zero = np.where(dist == 0)

        dist_to_last = np.copy(dist)
        for i, j in zip(*index_dist_is_zero):
            dist_to_last[i, j] = dist_to_last[i - 1, j]

        dist_to_next = np.copy(dist)
        for i, j in reversed(list(zip(*index_dist_is_zero))):
            dist_to_next[i, j] = dist_to_next[i + 1, j]

        #--- normalize all the distances
        norm = np.max(F, axis=0) - np.min(F, axis=0)
        norm[norm == 0] = np.nan
        dist_to_last, dist_to_next = dist_to_last[:-1] / norm, dist_to_next[1:] / norm

        #--- if we divided by zero because all values in one columns are equal replace by none
        dist_to_last[np.isnan(dist_to_last)] = 0.0
        dist_to_next[np.isnan(dist_to_next)] = 0.0

        #--- sum up the distance to next and last and norm by objectives - also reorder from sorted list
        J = np.argsort(I, axis=0)
        crowding = np.sum(dist_to_last[J, np.arange(n_obj)] + dist_to_next[J, np.arange(n_obj)],         axis=1) / n_obj

    #--- replace infinity with a large number
    crowding[np.isinf(crowding)] = infinity

    return crowding


def nsganet(
        pop_size=100,
        sampling=RandomSampling(var_type=np.int),
        selection=TournamentSelection(func_comp=binary_tournament),
        crossover=PointCrossover(n_points=2),
        mutation=PolynomialMutation(eta=3, var_type=np.int),
        eliminate_duplicates=True,
        n_offsprings=None,
        **kwargs):
    """

    Parameters
    ----------
    pop_size : {pop_size}
    sampling : {sampling}
    selection : {selection}
    crossover : {crossover}
    mutation : {mutation}
    eliminate_duplicates : {eliminate_duplicates}
    n_offsprings : {n_offsprings}

    Returns
    -------
    nsganet : :class:`~pymoo.model.algorithm.Algorithm`
        Returns an NSGANet algorithm object.


    """

    return NSGANet(pop_size=pop_size,
                   sampling=sampling,
                   selection=selection,
                   crossover=crossover,
                   mutation=mutation,
                   survival=RankAndCrowdingSurvival(),
                   eliminate_duplicates=eliminate_duplicates,
                   n_offsprings=n_offsprings,
                   **kwargs)


############################################################################################
#                                   Visualization
############################################################################################

def visualize_macro(genome, rankdir="UD", format="pdf", title=None, filename="genome", type="residual"):
    """
    Graphviz representation of network created by genome.
    :param genome: list of lists.
    :param rankdir: direction graph is oriented "UD"=Vertical, "LR"=horizontal.
    :param format: output file format, jpg, png, etc.
    :param title: title of graph.
    :param filename: filename of graph.
    :param type: string, what kind of decoder should we use.
    :return: graphviz dot object.
    """
    node_color = "lightblue"
    conv1x1_color = "white"
    sum_color = "green4"
    pool_color = "orange"
    phase_background_color = "gray"
    fc_color = "gray"

    node_shape = "circle"
    conv1x1_shape = "doublecircle"

    structure = []

    # Build node ids and names to make building graph easier.
    for i, gene in enumerate(genome):
        all_zeros = sum([sum(t) for t in gene[:-1]]) == 0

        if all_zeros:
            continue  # Skip everything is a gene is all zeros.

        prefix = "gene_" + str(i)
        phase = ("cluster_" + str(i + 1), "Phase " + str(i + 1))

        nodes = [(prefix + "_node_0", ' ')] \
            + [(prefix + "_node_" + str(j + 1), ' ') for j in range(len(gene) + 1)]

        pool = (prefix + "_pool", "Pooling")


        edges = []
        graph = ResidualPhase.build_dependency_graph(gene)

        for sink, dependencies in graph.items():
            for source in dependencies:
                edges.append((nodes[source][0], nodes[sink][0]))

        structure.append(
            {
                "nodes": nodes,
                "edges": edges,
                "pool": pool,
                "phase": phase,
                "all_zeros": all_zeros,
                "graph": graph
            }
        )

    final_pool = structure[-1]["pool"]
    new_pool = (final_pool[0], "Avg. Pooling")
    structure[-1]["pool"] = new_pool

    node_attr = dict(style='filled',
                     shape='box',
                     align='left',
                     fontsize='12',
                     ranksep='0.1',
                     height='0.2')
    dot = Digraph(format=format, filename=filename+'.gv', node_attr=node_attr, graph_attr=dict(size="12, 12"))
    dot.attr(rankdir=rankdir)

    if title:
        dot.attr(label=title+"\n\n")
        dot.attr(labelloc='t')

    dot.node(str("input"), "Input")


    for j, struct in enumerate(structure):
        nodes = struct['nodes']
        edges = struct['edges']
        phase = struct['phase']
        pool = struct['pool']
        graph = struct['graph']
        all_zeros = struct['all_zeros']

        # Add nodes.
        dot.node(nodes[0][0], nodes[0][1], fillcolor=conv1x1_color, shape=conv1x1_shape)

        if j > 0:
            dot.edge(structure[j - 1]['pool'][0], nodes[0][0])

        if not all_zeros:
            with dot.subgraph(name=phase[0]) as p:
                p.attr(fillcolor=phase_background_color, label='', fontcolor="black", style="filled")

                for i in range(1, len(nodes) - 1):
                    if len(graph[i]) != 0:
                        p.node(nodes[i][0], nodes[i][1], fillcolor=node_color, shape=node_shape)

        dot.node(nodes[-1][0], nodes[-1][1], fillcolor=sum_color, shape=node_shape)
        dot.node(*pool, fillcolor=pool_color)

        # Add edges.
        for edge in edges:
            dot.edge(*edge)

        dot.edge(nodes[-1][0], pool[0])

    dot.edge("input", structure[0]['nodes'][0][0])

    dot.node("linear", "Linear", fillcolor=fc_color)
    dot.edge(structure[-1]['pool'][0], "linear")

    dot.view()
    return dot


############################################################################################
#                                Nas Component
############################################################################################
class NAS(Problem):
    """
    This class define the Problem as required by PYMOP
    """
    def __init__(
        self,
        run_id : str,
        seed: int,
        train_dataset : Dataset,
        valid_dataset : Dataset,
        search_space : str ='NSGA_micro',
        n_var : int = 20,         #number of variables
        n_obj : int = 1,          #number of objectives
        n_constr : int = 0,       #number of constraints
        lb : int = None,          #Lower bounds for the variables.
        ub : int = None,          #upper bounds for the variable.
        init_channels : int = 24,
        layers : int = 8,
        epochs : int = 20,
        batch_size : int = 128,
        logger = None,
        total_runs : int = 1000,
    ):
        super().__init__(n_var=n_var, n_obj=n_obj, n_constr=n_constr, type_var=np.int)
        self.run_id = run_id
        self.seed = seed
        self.xl = lb
        self.xu = ub
        self.search_space = search_space
        self.init_channels = init_channels
        self.layers = layers
        self.epochs = epochs
        self.n_evaluated = 0  # keep track of how many architectures are sampled
        self.total_runs = total_runs
        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset
        self.batch_size = batch_size
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        self.logger = logger

    def add_flops_counting_methods(self, net_main_module):
         """
         Adding additional methods to the existing module object,
         this is done this way so that each function has access to self object
         TODO: improve the way this is stored
         """
         net_main_module.start_flops_count = start_flops_count.__get__(net_main_module)
         net_main_module.stop_flops_count = stop_flops_count.__get__(net_main_module)
         net_main_module.reset_flops_count = reset_flops_count.__get__(net_main_module)
         net_main_module.compute_average_flops_cost = compute_average_flops_cost.__get__(net_main_module)

         net_main_module.reset_flops_count()

         # Adding variables necessary for masked flops computation
         net_main_module.apply(add_flops_mask_variable_or_reset)

         return net_main_module

    def search(
        self,
        genome,
        epochs,
        search_space='NSGA_micro',
        save='Design_1',
        seed=0,
        init_channels=24,
        layers=11,
        auxiliary=False,
        cutout=False,
        drop_path_prob=0.0,
        memory: CollectiveMemory = None,
    ):

        #--- For time tracking
        start = time.time()

        # ---- train logger ----------------- #
        save_pth = os.path.join(f"{self.run_id}/search-{save}")
        if not os.path.exists(save_pth):
            os.mkdir(save_pth)

        #--- make it array for easy store
        genome = np.array(genome)

        # ---- parameter values setting ----- #
        if search_space == 'NSGA_micro':
            genotype = decode_micro(genome)
            model = MicroNetwork(init_channels, self.train_dataset.n_classes, layers, auxiliary, genotype)
        elif search_space == 'NSGA_macro':
            genotype = decode_macro(genome)
            channels = [(1, init_channels),
                        (init_channels, 2*init_channels),
                        (2*init_channels, 4*init_channels)]
            model = MacroNetwork(genotype, channels, self.train_dataset.n_classes, (self.train_dataset.img_rows, self.train_dataset.img_cols), decoder='residual')

        else:
            raise NameError('Unknown search space type')


        #--- Get the recipe of the best performing run
        config = memory.complete_config({
            'genome':str(genome.tolist()),
            'init_channels':init_channels,
            'layers':layers,
            'NAS': search_space,
        })

        #torch.cuda.set_device(self.device)
        #cudnn.benchmark = True
        #torch.manual_seed(seed)
        #cudnn.enabled = True
        #torch.cuda.manual_seed(seed)

        n_params = (np.sum(np.fromiter((np.prod(v.size()) for v in filter(lambda p: p.requires_grad, model.parameters())), float)) / 1e6)
        model = model.to(self.device)

        #--- read the desired data
        #--- Enable support for preprocessing
        transformations = [transforms.ToTensor()]
        for transform in memory.preprocessing:
            if transform in config and config[transform]:
                if 'GaussianBlur' in transform:
                    transformations.insert(0,GaussianBlur())
                if 'CV2threshold' in transform:
                    transformations.insert(0,CV2threshold())
                if 'medianBlur' in transform:
                    transformations.insert(0,medianBlur())
        if len(transformations) > 2:
            self.train_dataset.transform = torchvision.transforms.Compose(transformations)
            self.valid_dataset.transform = torchvision.transforms.Compose(transformations)

        train_loader = DataLoader(dataset=self.train_dataset,
                          batch_size=int(self.batch_size),
                          pin_memory=True,
                          shuffle=True)
        valid_loader = DataLoader(dataset=self.valid_dataset,
                         batch_size=int(self.batch_size),
                         pin_memory=True,
                         shuffle=False)

        #--- Check if there is a similar run to warm start weights
        #--- create an search_config to search for similar runs with same
        #--- architectural config to proactively use
        search_config = {'genome':str(genome.tolist()), "init_channels": init_channels, "layers":layers, "NAS":search_space}
        similar_runs = memory.get_runs_by_config(search_config,filters=['save_path'])
        if similar_runs.size:
            self.logger.debug(f"Found a similar run to {config} being {similar_runs}")
            model.warmstart_weights(similar_runs)

        #--- Train the model
        accuracy_history = train_fn(model, config, train_loader, self.device, self.epochs, droprate = drop_path_prob)

        #--- Get how good was this model
        valid_acc = eval_fn(model,loader=valid_loader,device=self.device)

        #--- If it is a full budget run, save it
        save_path = None
        if valid_acc > memory.highest_score*0.9:
            os.makedirs('models', exist_ok=True)
            save_path = 'NSGA_%.3f_'%(valid_acc) + str(os.getpid()) + "_" + str(calendar.timegm(time.  gmtime())) + '.pt'
            save_path  = os.path.join('models', save_path)
            torch.save(model, save_path)

        config['budget'] = epochs
        config['accuracy'] = valid_acc
        config['save_path'] = save_path
        config['accuracy_history'] = ''.join('%.3f,'%(e) for e in accuracy_history)
        config['start'] = start
        config['end'] = time.time()
        config['seed'] = self.seed
        config['method'] = self.run_id
        self.logger.info(f"config={config}")

        memory.addConfigMemory(config)

        #--- calculate for flops
        model = self.add_flops_counting_methods(model)
        model.eval()
        model.start_flops_count()
        random_data = torch.randn(1, 1, 28, 28)
        model(torch.autograd.Variable(random_data).to(self.device))
        n_flops = np.round(model.compute_average_flops_cost() / 1e6, 4)
        self.logger.info(f"SEARCH: search_space={search_space} genotype={genotype} Architecture={genotype} param_size={n_params} valid_acc={valid_acc} n_flops={n_flops}")

        #--- save to file
        with open(os.path.join(save_pth, 'log.txt'), "w") as file:
            file.write("Genome = {}\n".format(genome))
            file.write("Architecture = {}\n".format(genotype))
            file.write("param size = {}MB\n".format(n_params))
            file.write("flops = {}MB\n".format(n_flops))
            file.write("valid_acc = {}\n".format(valid_acc))

        return {
            'valid_acc': valid_acc,
            'params': n_params,
            'flops': n_flops,
        }


    def _evaluate(self, x, out, *args, **kwself):
        """
        Evaluate the given problem.
        The function values set as defined in the function.
        The constraint values are meant to be positive if infeasible. A higher positive values means "more" infeasible".
        If they are 0 or negative, they will be considered as feasible what ever their value is.
        Parameters
        ----------
        X : np.array
            A two dimensional matrix where each row is a point to evaluate and each column a variable.
        return_as_dictionary : bool
            If this is true than only one object, a dictionary, is returned. This contains all the results
            that are defined by return_values_of. Otherwise, by default a tuple as defined is returned.
        return_values_of : list of strings
            You can provide a list of strings which defines the values that are returned. By default it is set to
            "auto" which means depending on the problem the function values or additional the constraint violation (if
            the problem has constraints) are returned. Otherwise, you can provide a list of values to be returned.
            Allowed is ["F", "CV", "G", "dF", "dG", "dCV", "hF", "hG", "hCV", "feasible"] where the d stands for
            derivative and h stands for hessian matrix.
        """
        #--- Speak the same language across components
        memory = CollectiveMemory()

        objs = np.full((x.shape[0], self.n_obj), np.nan)

        self.logger.info(f"EVALUATE-START: with x={x} and x_shape={x.shape}")

        for i in range(x.shape[0]):
            arch_id = self.n_evaluated + 1
            self.logger.info(f"EVALUATE-ITER={arch_id}/{self.total_runs}")

            #--- call back-propagation training
            if self.search_space == 'NSGA_micro':
                genome = convert_micro(x[i, :])
            elif self.search_space == 'NSGA_macro':
                genome = convert_macro(x[i, :])

            #--- Train a model and add results to memory
            performance = self.search(
                genome=genome,
                search_space=self.search_space,
                init_channels=self.init_channels,
                layers=self.layers, cutout=False,
                epochs=self.epochs,
                save='arch_{}'.format(arch_id),
                memory = memory,
            )

            #--- all objectives assume to be MINIMIZED !!!!!
            objs[i, 0] = 100 - performance['valid_acc']
            objs[i, 1] = performance['flops']

            self.n_evaluated += 1

        #--- Save any new knowledge
        memory.save()

        out["F"] = objs
        # if your NAS problem has constraints, use the following line to set constraints
        # out["G"] = np.column_stack([g1, g2, g3, g4, g5, g6]) in case 6 constraints


# ---------------------------------------------------------------------------------------------------------
# Define what statistics to print or save for each generation
# ---------------------------------------------------------------------------------------------------------
def do_every_generations_callback(logger):
    def do_every_generations(algorithm):
        # this function will be call every generation
        # it has access to the whole algorithm class
        gen = algorithm.n_gen
        pop_var = algorithm.pop.get("X")
        pop_obj = algorithm.pop.get("F")

        logger.info(f"GENERATION={gen} pop_var={pop_var} pop_obj={pop_obj}")

        # report generation info to files
        logger.info("generation = {}".format(gen))
        logger.info("population error: best = {}, mean = {}, "
                     "median = {}, worst = {}".format(np.min(pop_obj[:, 0]), np.mean(pop_obj[:, 0]),
                                                      np.median(pop_obj[:, 0]), np.max(pop_obj[:, 0])))
        logger.info("population complexity: best = {}, mean = {}, "
                     "median = {}, worst = {}".format(np.min(pop_obj[:, 1]), np.mean(pop_obj[:, 1]),
                                                      np.median(pop_obj[:, 1]), np.max(pop_obj[:, 1])))
    return do_every_generations

class NSGAComponent(Process):
    def __init__(
        self,
        run_id : str,
        iterations : int,
        dataset : str,
        seed: int,
        max_budget : int,
        queue: Queue,
        dataset_reduction : float = None,
        data_dir : str = '../data',
        overwrite : bool = False,
        previous_run : str = None,
        config_type: str = 'NSGA_macro',
        **kwself,
    ):
        """
        NSGA object that is supposed to run in a separated CPU with a run method
        """
        super(NSGAComponent, self).__init__()

        #--- arguments for micro search space
        self.n_blocks = 5
        self.n_ops = 9
        self.n_cells = 2

        #--- arguments for macro search space
        self.n_nodes = 4

        #--- hyper-parameters for algorithm
        #--- the number of generations. First one is the pop size, then offspring
        self.iterations = iterations
        self.pop_size = 20
        self.n_offspring = 7

        if config_type == 'NSGA_micro':
            #--- NASNet search space but is heavely expensive
            self.pop_size = 10
            self.n_offspring = 3

        #--- arguments for back-propagation training during search
        self.init_channels = 24
        self.layers = 2

        #--- Handle the dataset to be created
        self.dataset = dataset
        self.data_dir = data_dir
        self.dataset_reduction = dataset_reduction
        self.load_data()

        #--- get all runs that have been launched as a pandas frame
        self.run_id = run_id
        self.max_budget = max_budget
        self.seed = seed
        self.previous_run = previous_run
        self.queue = queue
        self.search_space = config_type
        self.w = None
        self.run_dir = f"{self.run_id}"
        if os.path.exists(self.run_dir):
            if overwrite:
                shutil.rmtree(self.run_dir)
            else:
                raise Exception(f"A run already exists at {self.run_dir} and overwrite is set to false")
        os.makedirs(self.run_dir, exist_ok=True)

        #--- track through logger
        self.logger_file = f"{self.run_id}/{self.run_id}_status.log"
        self.logger = None
        self.configure_logger_for_status()


    def load_data(self):
        """Loads the data of the model"""
        data_augmentations = transforms.ToTensor()
        if self.dataset == 'K49':
            self.train_dataset = K49(self.data_dir, data_augmentations, self.dataset_reduction, 'train')
            self.valid_dataset = K49(self.data_dir, data_augmentations, self.dataset_reduction, 'valid')
        elif self.dataset == 'KMNIST':
            self.train_dataset = KMNIST(self.data_dir, data_augmentations, self.dataset_reduction, 'train')
            self.valid_dataset = KMNIST(self.data_dir, data_augmentations, self.dataset_reduction, 'valid')
        else:
            raise Exception("Unsuported dataset provided not in {K49, KMNIST}")

    def configure_logger_for_status(self):
        """Configure all logging to be saved on a standarized logger"""
        self.logger = logging.getLogger('status')
        self.logger.setLevel(logging.INFO)
        fh = logging.FileHandler(self.logger_file)
        fh.setLevel(logging.INFO)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

    def get_iteration(self):
        """Get the current iteration for the progress bar tracking"""
        if not os.path.isfile(self.logger_file):
            return 0

        generation = 0
        with open(self.logger_file, 'r') as f:
            lines = f.read().splitlines()
            for line in lines:
                if "GENERATION" in line:
                    matching = [s for s in line.split() if "GENERATION"  in s][0]
                    generation = int(matching.split("=")[-1])
        return generation

    def run(self):
        """Overwrite Method to allow the NSGA object to run"""

        self.logger.info(f"Started a NSGA process called {self.run_id} with number {self.pid} and max_budget={self.max_budget}")

        #--- Looking for reproducibility
        make_reproducible_run(self.seed)

        # setup NAS search problem
        if self.search_space == 'NSGA_micro':  # NASNet search space
            n_var = int(4 * self.n_blocks * 2)
            lb = np.zeros(n_var)
            ub = np.ones(n_var)
            h = 1
            for b in range(0, n_var//2, 4):
                ub[b] = self.n_ops - 1
                ub[b + 1] = h
                ub[b + 2] = self.n_ops - 1
                ub[b + 3] = h
                h += 1
            ub[n_var//2:] = ub[:n_var//2]
        elif self.search_space == 'NSGA_macro':  # modified GeneticCNN search space
            n_var = int(((self.n_nodes-1)*self.n_nodes/2 + 1)*3)
            lb = np.zeros(n_var)
            ub = np.ones(n_var)
        else:
            raise NameError(f"Unknown search space type provided in config={self.search_space}")

        problem = NAS(
            run_id = self.run_id,
            seed = self.seed,
            n_var=n_var,
            search_space=self.search_space,
            n_obj=2,
            n_constr=0,
            lb=lb,
            ub=ub,
            init_channels=self.init_channels,
            layers=self.layers,
            epochs=self.max_budget,
            train_dataset = self.train_dataset,
            valid_dataset = self.valid_dataset,
            logger = self.logger,
            total_runs = self.pop_size + (self.iterations-1) * self.n_offspring,
        )

        self.logger.info(f"STARTED-RUN: iterations={self.iterations} pop_size={self.pop_size} n_offsprings={self.n_offspring} n_offspring={self.n_offspring}")

        #--- configure the nsga-net method
        method = nsganet(pop_size=self.pop_size,
                                n_offsprings=self.n_offspring,
                                eliminate_duplicates=True)

        #--- Configure the callback to dynamically right to our file
        do_every_generations = do_every_generations_callback(self.logger)

        res = minimize(problem,
                       method,
                       callback=do_every_generations,
                       termination=('n_gen', self.iterations))

        return


if __name__ == "__main__":
    q = Queue()
    for config_type in ['NSGA_macro', 'NSGA_micro']:
        print(f"Working on component {config_type}")
        NAS_NSGA = NSGAComponent(
            run_id =  "test-" + config_type,
            iterations = 1,
            dataset = 'KMNIST',
            seed = 56,
            max_budget = 1,
            queue = q,
            dataset_reduction = 0.99,
            config_type = config_type,
            overwrite = True,
        )

        NAS_NSGA.pop_size = 1
        NAS_NSGA.iterations   = 1
        NAS_NSGA.n_offspring = 1
        NAS_NSGA.init_channels = 24
        NAS_NSGA.layers = 2
        NAS_NSGA.run()

