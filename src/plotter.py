from CollectiveMemory import CollectiveMemory
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
import seaborn as sns
import pandas as pd
sns.set_style('whitegrid')


COLOR = ['#2C3531', '#116466', '#D9B08C', '#FFCB9A', '#D1E8E2']
COLOR = {'incumbent': '#2C3531', 'levels':['#116466', '#D9B08C', '#FFCB9A', '#D1E8E2'], 'grid':'#2C3531'}
COLOR = {'incumbent': '#E27D60', 'levels':['#85DCB0', '#E8A87C', '#C38D9E', '#41B3A3'], 'grid':'#2C3531'}
COLOR = {'incumbent': '#b2182b', 'levels':['#e0e0e0', '#fddbc7', '#878787', '#f4a582'], 'grid':'#4d4d4d', 'methods':['#053061', '#4393c3', '#d1e5f0', '#fee090', '#abdda4']}

COLOR = {'incumbent': '#d73027', 'levels':['#e0f3f8', '#abd9e9', '#74add1', '#4575b4'], 'grid':'#4d4d4d', 'methods':['#fdae61', '#66bd63', '#66c2a5', '#de77ae', '#7fbc41']}
#list_of_databases = ["../tables_sql/memory_11.db", "../tables_sql/memory_73.db", "../tables_sql/memory_78.db", "../tables_sql/memory_93.db", "../tables_sql/memory_m1.db"]
def plot_acc_all(list_of_databases):
    """Create the final plots for the project"""
    fig, axis = plt.subplots()

    #--- Get the list of pandas databases
    list_of_pandas_db = []
    for memory in list_of_databases:
        list_of_pandas_db.append(CollectiveMemory(memory_name=memory).memory)

    #--- Plot the accuracies
    list_of_accuracies = []
    for memory in list_of_pandas_db:
        list_of_accuracies.append(memory.sort_values(by=["start"])[["accuracy"]])

    plot_incumbents_agg(axis, list_of_accuracies)

    #--- Plot Levels
    plot_level_information(axis,list_of_pandas_db)

    #--- The method accuracies
    plot_method_accuracies(axis, list_of_pandas_db)

    #--- Comply with best practices
    axis.set_ylim([80,98])
    axis.set_xlim([0,303])
    plt.legend(fontsize=16)
    axis.grid(True, which="both", ls="-", alpha=0.5, color=COLOR['grid'])
    plt.xlabel('Iteration', fontsize=18)
    plt.ylabel('Accuracy', fontsize=18)
    plt.title('Incumbent over multiple levels', fontsize=24)
    plt.tight_layout()
    plt.savefig('mean_incumbents.pdf')
    plt.show()

def plot_method_accuracies(axis, list_of_pandas_db):
    """Plot a list method vs accuracies"""

    #--- stack all values and print everything
    x, y, methods = [], [], []
    for memory in list_of_pandas_db:
        y.append(memory.sort_values(by=["start"])[["accuracy"]].values)
        x.append(np.arange(y[-1].size))
        methods.append(memory.sort_values(by=["start"])[["method"]].values)

    x = np.hstack(x)
    y = np.vstack(y).squeeze()
    methods = np.vstack(methods).squeeze()

    #--- For this plot we don't care about the level
    def remove_level(a):
        return a[:-3]
    vremove_level = np.vectorize(remove_level)
    print(f"Before={methods}")
    methods = vremove_level(methods)
    print(f"after={methods}")

    labels = np.unique(methods)
    method2color = {}
    for label in labels:
        if label not in method2color:
            method2color[label] = COLOR['methods'][len(list(method2color.keys()))]

    #---
    for method in labels:
        plt.scatter(
                x[methods==method],
                y[methods==method],
                color=method2color[method],
                label=method,
        )
    return

def plot_level_information(axis, list_of_pandas_db):
    """Plots information about where levels took place"""
    levels = defaultdict(dict)

    for memory in list_of_pandas_db:

        #--- The level is embbeded in the name
        current_level = None
        for i, method in enumerate(memory.sort_values(by=["start"])[["method"]].values):
            level = method[0][-2:]

            #--- Register changes to see when is the last iteration
            last = i == len(memory.sort_values(by=["start"])[["method"]].values) - 1
            if (current_level and current_level != level) or last:
                levels[current_level]["end"] = i

            #--- Found a new level
            if level not in levels:
                levels[level]["start"] = i
                current_level = level

        #--- just need it for one level, all should be same
        break

    #--- Assign a color and plot!
    for i, level in enumerate(list(levels.keys())):
        levels[level]['color'] = COLOR['levels'][i]
        props = dict(boxstyle='round', facecolor=levels[level]['color'], alpha=0.6)
        axis.axvspan(levels[level]["start"], levels[level]["end"], facecolor=levels[level]['color'], alpha=0.3)
        axis.text(
                ( (levels[level]["end"] - levels[level]["start"])/2 + levels[level]["start"]),
                88,
                level,
                fontsize=18,
                verticalalignment='top',
                bbox=props
        )
    return

def plot_incumbents_agg(axis, list_of_accuracies):
    """
    Function to plot the mean over the regret
    """

    #--- Get the incumbent to plot
    y_incumbents = np.stack(
       [np.maximum.accumulate(list_of_accuracies[i]) for i in range(len(list_of_accuracies))],
       axis=0
    )

    #--- Plot uncertainty and mean
    mean, std = np.mean(y_incumbents, axis=0), np.std(y_incumbents, axis=0)
    plt.plot(np.arange(len(mean)), mean, drawstyle='steps-post', label="Incumbent", color=COLOR['incumbent'], linewidth=3)
    plt.fill_between(np.arange(len(mean)), (mean - std).squeeze(), (mean + std).squeeze(), alpha=0.3, color=COLOR['incumbent'])
    axis.set_ylim([90,98])
    axis.set_xlim([0,303])

    return
dict_of_databases = {
        'level': ["../tables_sql/memory_11.db", "../tables_sql/memory_73.db", "../tables_sql/memory_78.db", "../tables_sql/memory_93.db", "../tables_sql/memory_m1.db"],
        'just_nsga': ['../tables_sql/just_NSGA/memory_39.db', '../tables_sql/just_NSGA/memory_73.db'],
        'just_BOHB': ['../tables_sql/just_BOHB/run1.txt', '../tables_sql/just_BOHB/run2.txt', '../tables_sql/just_BOHB/run3.txt']
}
def plot_just_graph_all(dict_of_databases):
    """Create the final plots for the project"""
    fig, axis = plt.subplots()

    #--- Get the list of pandas databases
    dict_of_pandas_db = defaultdict(list)
    for db_type, list_of_databases in dict_of_databases.items():
        for memory in list_of_databases:
            if 'BOHB' in db_type:
                dict_of_pandas_db[db_type].append(pd.read_csv(memory, index_col=None))
                dict_of_pandas_db[db_type][-1][["accuracy"]] = dict_of_pandas_db[db_type][-1][["accuracy"]] * 100
                continue
            dict_of_pandas_db[db_type].append(CollectiveMemory(memory_name=memory).memory)
            print(f"Read a db called {memory} with {CollectiveMemory(memory_name=memory).memory.shape}")

    #--- We have to recreate accumulative runtime
    min_num_points = 9999
    data_to_plot =[]
    for db_type, list_of_pandas_db in dict_of_pandas_db.items():
        list_of_accuracies = []
        list_of_cummulative_durations = []
        min_shape = 9999
        for memory in list_of_pandas_db:
            list_of_accuracies.append(memory.sort_values(by=["start"])[["accuracy"]])

            #--- Getting accumulative accuracies is tricky
            list_of_cummulative_durations.append(
                    np.cumsum(
                        memory.sort_values(by=["start"])[["end"]].values.astype(float) - memory.sort_values(by=["start"])[["start"]].values.astype(float)
                    )
            )

            if list_of_cummulative_durations[-1].shape[0] < min_shape:
                min_shape = list_of_cummulative_durations[-1].shape[0]

        #--- make sure shape is same for each element
        for i in range(len(list_of_cummulative_durations)):
            list_of_accuracies[i] = list_of_accuracies[i][0:min_shape]
            list_of_cummulative_durations[i] = list_of_cummulative_durations[i][0:min_shape]

        data_to_plot.append( {
            'label': db_type,
            'x': np.mean(np.vstack(list_of_cummulative_durations), axis=0),
            'y': np.stack(
                [np.maximum.accumulate(list_of_accuracies[i]) for i in range(len(list_of_accuracies))],
                axis=0
                )
            }
        )

        #--- Keep track of the minimum to make sure we only plot up to this points
        if min_shape < min_num_points:
            min_num_points = min_shape

    for i in range(len(data_to_plot)):
        #--- plot only the minimum time among all runs
        data_to_plot[i]['x'] = data_to_plot[i]['x'][0:min_num_points]
        mean, std = np.mean(data_to_plot[i]['y'], axis=0), np.std(data_to_plot[i]['y'], axis=0)
        mean = mean[0:min_num_points]
        std = std[0:min_num_points]
        print(F"mean= {mean.shape}")
        plt.plot(data_to_plot[i]['x'], mean, drawstyle='steps-post', label=data_to_plot[i]['label'], color=COLOR['methods'][i], linewidth=3)
        plt.fill_between(data_to_plot[i]['x'], (mean - std).squeeze(), (mean + std).squeeze(), alpha=0.3, color=COLOR['methods'][i])


    #--- Comply with best practices
    #axis.set_ylim([80,98])
    #axis.set_xlim([0,303])
    plt.legend(fontsize=16)
    axis.grid(True, which="both", ls="-", alpha=0.5, color=COLOR['grid'])
    plt.xlabel('Cumulative Runtime', fontsize=18)
    plt.ylabel('Accuracy', fontsize=18)
    plt.title(f"Incumbent for {min_num_points} Runs", fontsize=24)
    plt.tight_layout()
    plt.savefig('comparisson.pdf')
    plt.show()

#../tables_sql/just_NSGA/NAS_NSGA_macro_L0_status.log
def plot_flop_accuracy(file_name):
    """Create the final plots for the project"""

    #--- Get the data
    data = pd.read_csv(file_name, index_col=None)

    #--- Plot on different scales
    fig, ax1 = plt.subplots()

    color = COLOR['incumbent']
    ax1.set_xlabel('Iteration (individuals)', fontsize=18)
    ax1.set_ylabel('accuracy', color=color, fontsize=18)
    ax1.plot(data["accuracy"], color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.grid(True, which="both", ls="-", alpha=0.5, color=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = COLOR['levels'][-1]
    ax2.set_ylabel('Flop count / 1e6', color=color, fontsize=18)  # we already handled the x-label with ax1
    ax2.plot(data["n_flops"], color=color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.grid(True, which="both", ls="-", alpha=0.5, color=color)

    plt.title('Optimization Criterias across iterations', fontsize=24)
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.show()


def plot_learning_curve():
    plt.plot(accuracy_history[scores==0][0], color='r', label='bad_runs')
    plt.plot(accuracy_history[scores==1][0], color='g', label='good_runs')
    plt.xlabel("Epochs", fontsize=16)
    plt.ylabel("Accuracy", fontsize=16)
    plt.suptitle('LC Prediction Data', fontsize=20)
    plt.plot(accuracy_history[scores==0].T, color='r')
    plt.plot(accuracy_history[scores==1].T, color='g')
    plt.grid(True)
    plt.legend(loc='lower right', fontsize=14)
    plt.savefig('lcpredictor.pdf')
    plt.show()
