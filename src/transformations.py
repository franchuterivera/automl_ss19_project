import random
import numbers
from PIL import Image
import torch
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2

class GaussianBlur(object):

     def __init__(self,factor=(5,5)):
         self.factor = factor

     def __call__(self, input):
         return cv2.GaussianBlur(input, self.factor, 0)

class medianBlur(object):

     def __init__(self,factor=5):
         self.factor = factor

     def __call__(self, input):
         return cv2.medianBlur(input, self.factor)

class CV2threshold(object):

     def __init__(self):
        self.factor = 0

     def __call__(self, input):
        return cv2.threshold(input, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

# Display one image
def display_one(a, title1 = "Original"):
    plt.imshow(a), plt.title(title1)
    plt.xticks([]), plt.yticks([])
    plt.show()
# Display two images
def display(a, b, title1 = "Original", title2 = "Edited"):
    plt.subplot(121), plt.imshow(a), plt.title(title1)
    plt.xticks([]), plt.yticks([])
    plt.subplot(122), plt.imshow(b), plt.title(title2)
    plt.xticks([]), plt.yticks([])
    plt.show()

